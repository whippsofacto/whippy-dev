# Whippy Dev

A personal blog and projects site built with Next js.

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Repo Settings

Pipelines are configured to run on every branch push.

Production branches `develop` and `master` will kick off builds if all pipeline tests pass.

We use the Vercel webhook to handle deployments. We do not initiate builds on every single push which is the default behaviour for Vercel - GitLab webhook integrations. We allow the pipeline to run and then in the deployment step, we use a webhook to tell Vercel that we want to initiate a deployment. This allows us to run tasks in the pipe and prevent deployments triggering if any of the tasks fail. Only builds from `master` will result in a deployment to the production env e.g. accessible at [https://whipp.dev].
