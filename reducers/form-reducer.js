const formReducer = (state, action) => {
  switch (action.type) {
    case 'HANDLE_INPUT_TEXT':
      return {
        ...state,
        [action.field]: action.payload,
      };
    case 'SET_FORM_VALUES':
      return {
        ...action.payload,
      };
    default:
      return state;
  }
};

const formInputHandler = (e, dispatch, options = {}) => {
  let field;
  if (options.fieldName) {
    field = options.fieldName;
  } else {
    field = e.target.name;
  }

  let payload;
  if (options.richText) {
    payload = e.target.getContent();
  } else if (options.valueNotEvent) {
    payload = e;
  } else if (options.checkbox) {
    payload = e.target.checked;
  } else {
    payload = e.target.value;
  }

  return dispatch({
    type: 'HANDLE_INPUT_TEXT',
    field,
    payload,
  });
};

const setFormState = (payload, dispatch) => dispatch({
  type: 'SET_FORM_VALUES',
  payload,
});

const formErrorsReducer = (state, action) => {
  switch (action.type) {
    case 'CREATE_ERROR':
      return {
        ...state,
        [action.field]: action.payload,
      };
    case 'CLEAR_ERROR':
      return {
        ...state,
        [action.field]: null,
      };
    default:
      return state;
  }
};

const createFormErrorsHandler = (fieldName, errorMessage, dispatch) => dispatch({
  type: 'CREATE_ERROR',
  field: fieldName,
  payload: errorMessage,
});

const clearFieldError = (fieldName, state, dispatch) => {
  if (state[fieldName]) {
    dispatch({
      type: 'CLEAR_ERROR',
      field: fieldName,
    });
  }
};

module.exports = {
  formReducer,
  formInputHandler,
  formErrorsReducer,
  createFormErrorsHandler,
  clearFieldError,
  setFormState,
};
