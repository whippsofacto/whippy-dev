import LottieControl from 'components/core/lottie/LotteControl';
import animationData from 'assets/lottie/ufo-error-page-404.json';
import Container from 'components/core/layouts/Container';
import InternalLink from 'components/core/links/internal-link';

export default function Custom404() {
  return (
    <Container>
      <div className="flex flex-col justify-center items-center min-h-screen text-center">
        <h4> 400000000000004 Light Years Away...</h4>
        <p> From anything resembling a page.</p>
        <LottieControl
          animationData={animationData}
          height={360}
        />
        <InternalLink className="underline text-indigo-600" href="/">
          Take Me Home.
        </InternalLink>
      </div>
    </Container>
  );
}
