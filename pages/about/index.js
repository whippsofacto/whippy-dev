import Container from 'components/core/layouts/SlimContainer';
import Main from 'components/core/layouts/Main';
import Header from 'components/core/headers/page-header';
import Image from 'components/core/image/Image';
import whippy from 'assets/whippy.jpg';
import pattern from 'assets/boy-fox-mole-bg-min.png';
import LottieControl from 'components/core/lottie/LotteControl';
import animationData from 'assets/lottie/abstract-circle.json';
import abstractWhatever from 'assets/lottie/dot-array.json';

const About = () => (
  <Main>
    <div>
      <Header
        backgroundImage={pattern}
        heading="About"
      />
      <Container>
        <div
          className="flex flex-col leading-relaxed"
        >
          <div className="flex-1 lg:p-12 flex justify-center my-8">
            <div className="relative">
              <div className="absolute z-0" style={{ right: '-74px', top: '-74px' }}>
                <LottieControl animationData={animationData} />
              </div>
              <div className="absolute z-0 opacity-40" style={{ top: '66px', left: '-80px' }}>
                <LottieControl animationData={abstractWhatever} />
              </div>
              <Image src={whippy} alt="whippy" className="rounded-full sm:w-80 sm:h-80 w-60 h-60 relative z-10" />
            </div>
          </div>
          <div className="flex-1 lg:py-12 lg:pr-12 mb-12 lg-mb-0">
            <h3 className="mb-8">
              &quot;Our cat is vicious and I need to keep him in biscuits.&quot;
            </h3>
            <p className="mb-6">
              {' '}
              When I started making this page, I didn&apos;t really know what to write,
              I started by thinking, what
              kind of things should I mention? Probably things like, which
              {' '}
              <span className="border-b-2 border-primary">
                languages,
                frameworks and tools
              </span>
              {' '}
              do I know and use?
            </p>
            <div className="flex pl-6 mb-8">
              <ul className="flex-1 list-disc">
                <li> HTML </li>
                <li> CSS3 </li>
                <li> React </li>
                <li> PHP </li>
                <li> MySQL </li>
                <li> Cypress </li>
              </ul>
              <ul className="flex-1 list-disc">
                <li> Jest </li>
                <li> AWS </li>
                <li> Git </li>
                <li> Sass </li>
                <li> CI </li>
                <li> Jira </li>
              </ul>
              <ul className="flex-1 list-disc">
                <li> JQuery </li>
                <li> Tailwind </li>
                <li> Bootstrap </li>
                <li> Ubuntu </li>
                <li> Docker </li>
                <li> Next</li>
              </ul>
            </div>
            <p className="mb-8">
              {' '}
              Then I wondered whether I should talk a litle bit about some of the experiences
              I&apos;ve had in the last
              {' '}
              <span className="border-b-2 border-primary">3.5 years working as a web developer</span>
              .
              Things like working on front and backend web projects for a
              diverse range of clients.
              Interpreting client requirements
              {' '}
              and translating them into estimated user stories.
              {' '}
              <span className="border-b-2 border-indigo-600">
                {' '}
                Making many, many mistakes
              </span>
              {' '}
              along the way but learning from each of them.
            </p>
            <p className="mb-8">
              I like to
              {' '}
              <span className="border-b-2 border-primary">
                do simple things well
              </span>
              {' '}
              and take pride in my work as much as I can.
              Some of the best experiences come from working with a team to
              overcome technical challenges. I try to keep
              the code I write readable, maintainable and testable.
              I try to be approachable and often,
              when I don&apos;t know an answer I&apos;ll try to find out or
              speak to someone who does.
            </p>
            <p className="mb-8">
              I try to remember that comparing ourselves to others is a waste of time.
            </p>
          </div>
        </div>
      </Container>
    </div>
  </Main>
);

export default About;
