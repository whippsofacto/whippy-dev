import LoginForm from 'components/authentication/LoginForm';

const LoginPage = () => (
  <div className="w-screen h-screen flex flex-col justify-center">
    <LoginForm />
  </div>
);

export default LoginPage;
