import { useEffect } from 'react';
import PropTypes from 'prop-types';
import userService from 'services/client/user/user';
import auth from 'lib/auth/auth';
import { useAuth } from 'context/auth';
import Router from 'next/router';
import CONSTS from 'lib/conts/system';
import Container from 'components/core/layouts/Container';
import LottieControl from 'components/core/lottie/LotteControl';
import animiationData from 'assets/lottie/morty-tears-loader.json';

const redirectOnFail = {
  redirect: {
    destination: CONSTS.REDIRECT_ON_FAIL_DEST,
    permanent: false,
  },
};

export const getServerSideProps = async (context) => {
  // Get the user's session based on the request

  let token;
  // if there is a new session token in the quer set that as token
  if (context.query.token) token = context.query.token;
  // if there's no query param, lets see if there's already a cookie set in the header,
  else if (context.req.headers.cookie) {
    token = auth.getCookieFromReqHeaders(context.req.headers.cookie, CONSTS.COOKIE_TOKEN);
  }

  // if there is no token, clear off
  if (!token) {
    return redirectOnFail;
  }

  try {
    const user = await userService.getUserFromToken(token);
    return {
      props: { user, token },
    };
  } catch (error) {
    return redirectOnFail;
  }
};

export default function Dashboard({ user, token }) {
  const { onValidLogin } = useAuth();

  useEffect(() => {
    setTimeout(() => {
      if (token) {
        onValidLogin(token, user);
        Router.push(CONSTS.REDIRECT_ON_SUCCESS_DEST);
      } else {
        Router.push(CONSTS.REDIRECT_ON_FAIL_DEST);
      }
    }, 5000);
  }, []);

  return (
    <Container>
      <div className="flex flex-col justify-center items-center min-h-screen text-center">
        <LottieControl animationData={animiationData} />
        <h3> Authororizing... </h3>
      </div>
    </Container>
  );
}

Dashboard.defaultProps = {
  token: undefined,
};

Dashboard.propTypes = {
  user: PropTypes.shape({
    email: PropTypes.string,
    username: PropTypes.string,
  }).isRequired,
  token: PropTypes.string,
};
