// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import knex from '../../knex/knex';

const faker = require('faker');

export default async (req, res) => {
  try {
    const env = process.env.NODE_ENV;
    const post = {
      title: `${env} -- ${faker.commerce.product()}`,
      content: `${faker.commerce.productDescription()}`,
    };
    await knex('posts').insert(post);
    res.status(200).json(post);
  } catch (err) {
    res.status(500).send(err);
  }
};
