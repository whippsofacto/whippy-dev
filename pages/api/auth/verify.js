const { verifyJwt } = require('lib/auth/auth');

export default async (req, res) => {
  if (req.method !== 'POST') return res.status(405).end();
  const { token } = req.body;

  if (!token) {
    return res.status(400)
      .send({ code: '400', message: 'could not verify user' });
  }

  const validToken = verifyJwt(req.body.token);
  if (!validToken) {
    return res.status(400)
      .send({ code: '400', message: 'invalid token' });
  }

  return res.status(200).send();
};
