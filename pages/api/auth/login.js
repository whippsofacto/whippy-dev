const { db } = require('lib/db/db');
const { validateEmail } = require('lib/validation/validation');
const { createAndSendUserAuthToken } = require('services/server/auth/auth-service');
const { ValidationError, BadMethodError } = require('lib/errors/errors');
const errorCodes = require('lib/errors/error-codes');
const { errorHandlerApi } = require('lib/errors/error-handling');

export default async (req, res) => {
  try {
    if (req.method !== 'POST') throw new BadMethodError();
    const { email } = req.body;

    if (!validateEmail(email)) {
      throw new ValidationError('not a valid email address', errorCodes.NOT_A_VALID_EMAIL_ADDRESS);
    }

    const user = await db().findOne('users', { email });

    if (user.length === 0) {
      throw new ValidationError('email address could not be found', errorCodes.REQUESTED_RESOURCE_COULD_NOT_BE_FOUND);
    }

    await createAndSendUserAuthToken(user);

    return res.status(204).send();
  } catch (error) {
    return errorHandlerApi(error, res);
  }
};
