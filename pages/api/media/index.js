import nextConnect from 'next-connect';

const multerConfig = require('lib/multer/multer');
const { BadMethodError } = require('lib/errors/errors');
const { isUserAuthorised } = require('lib/auth/auth');
const { errorHandlerApi } = require('lib/errors/error-handling');
const mediaService = require('services/server/media/media');

const handler = nextConnect()
  .use(multerConfig.uploadSingle)
  .post(async (req, res) => {
    try {
      if (req.method !== 'POST') throw new BadMethodError();
      isUserAuthorised(req, res);

      const media = await mediaService.uploadMediaToS3(req.file, 'media');

      return res.status(200).send({ id: media.id });
    } catch (error) {
      return errorHandlerApi(error, res);
    }
  });

export const config = {
  api: {
    bodyParser: false,
  },
};

export default handler;
