const { BadMethodError, ValidationError } = require('lib/errors/errors');
const { errorHandlerApi } = require('lib/errors/error-handling');
const mediaService = require('services/server/media/media');

export default async (req, res) => {
  try {
    if (req.method !== 'GET') throw new BadMethodError();
    const { query } = req;
    // eslint-disable-next-line radix
    const mediaId = parseInt(query.mediaId);

    if (Number.isNaN(mediaId)) {
      throw new ValidationError('media id is not valid.');
    }

    const media = await mediaService.getMediaFromS3(mediaId);
    return media.pipe(res);
  } catch (error) {
    return errorHandlerApi(error, res);
  }
};
