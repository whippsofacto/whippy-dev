const { validateEmail } = require('lib/validation/validation');
const { createContactEmail } = require('services/server/contact/contact');
const { ValidationError, BadMethodError } = require('lib/errors/errors');
const errorCodes = require('lib/errors/error-codes');
const { errorHandlerApi } = require('lib/errors/error-handling');

// const ALLOWED_KEYS = ['name', 'email', 'web', 'infra', 'seo', 'design', 'cuppa', 'info'];

export default async (req, res) => {
  try {
    if (req.method !== 'POST') throw new BadMethodError();

    if (!validateEmail(req.body.formData.email)) {
      throw new ValidationError('not a valid email address', errorCodes.NOT_A_VALID_EMAIL_ADDRESS, 'email');
    }

    if (req.body.formData.email.length > 255) {
      throw new ValidationError('name too long', errorCodes.EMAIL_FIELD_EXCEEDS_MAXIUM_CHARS, 'email');
    }

    if (!req.body.formData.name) {
      throw new ValidationError('form must contain a name', errorCodes.NO_NAME_FIELD, 'name');
    }

    if (req.body.formData.name.length > 255) {
      throw new ValidationError('name too long', errorCodes.NAME_FIELD_EXCEEDS_MAXIUM_CHARS, 'name');
    }

    if (typeof req.body.formData.web !== 'boolean') {
      throw new ValidationError('checkbox web must be a booolean', errorCodes.SHOULD_BE_BOOLEAN_VALUE);
    }

    if (typeof req.body.formData.infra !== 'boolean') {
      throw new ValidationError('checkbox infra must be a booolean', errorCodes.SHOULD_BE_BOOLEAN_VALUE);
    }

    if (typeof req.body.formData.seo !== 'boolean') {
      throw new ValidationError('checkbox migration must be a booolean', errorCodes.SHOULD_BE_BOOLEAN_VALUE);
    }

    if (typeof req.body.formData.design !== 'boolean') {
      throw new ValidationError('checkbox design must be a booolean', errorCodes.SHOULD_BE_BOOLEAN_VALUE);
    }

    if (typeof req.body.formData.cuppa !== 'boolean') {
      throw new ValidationError('checkbox cuppa must be a booolean', errorCodes.SHOULD_BE_BOOLEAN_VALUE);
    }

    await createContactEmail(req.body.formData);

    return res.status(204).send();
  } catch (error) {
    return errorHandlerApi(error, res);
  }
};
