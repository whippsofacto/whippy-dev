const shape = require('lib/shapes/shape-user');
const userService = require('services/server/user/user');
const { isUserAuthorised } = require('lib/auth/auth');
const { errorHandlerApi } = require('lib/errors/error-handling');

export default async (req, res) => {
  try {
    isUserAuthorised(req, res);
    const user = await userService.getUserById(req.user.id);

    const marshalledUser = shape.marshallUserToJson(user);

    return res.status(200).send(marshalledUser);
  } catch (error) {
    return errorHandlerApi(error, res);
  }
};
