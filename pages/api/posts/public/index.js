const { BadMethodError } = require('lib/errors/errors');

const { errorHandlerApi } = require('lib/errors/error-handling');
const postService = require('services/server/posts/posts');
const getPostShape = require('lib/shapes/shape-get-posts');

export default async (req, res) => {
  try {
    // get all posts for public consumption
    if (req.method !== 'GET') throw new BadMethodError();
    const posts = await postService.getPosts({ ...req.query, active: true });
    const marshallPosts = getPostShape.marshallPagedPostToJson(posts);
    return res.status(200).send(marshallPosts);
  } catch (error) {
    return errorHandlerApi(error, res);
  }
};
