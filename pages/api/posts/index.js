const { BadMethodError } = require('lib/errors/errors');
const { isUserAuthorised } = require('lib/auth/auth');
const { errorHandlerApi } = require('lib/errors/error-handling');
const postService = require('services/server/posts/posts');
const getPostShape = require('lib/shapes/shape-get-posts');

export default async (req, res) => {
  try {
    // create a new post
    if (req.method === 'POST') {
      isUserAuthorised(req, res);

      const newPostId = await postService.createNewPost(req.body, req.user.id);

      return res.status(201).send({ id: newPostId });
    }
    // get all posts
    if (req.method === 'GET') {
      isUserAuthorised(req, res);
      const posts = await postService.getPosts(req.query);
      const marshallPosts = getPostShape.marshallPagedPostToJson(posts);
      return res.status(200).send(marshallPosts);
    }
    // if neither, it's a bad requests
    throw new BadMethodError();
  } catch (error) {
    return errorHandlerApi(error, res);
  }
};
