import errorCodes from 'lib/errors/error-codes';

const { BadMethodError, ValidationError } = require('lib/errors/errors');
const { errorHandlerApi } = require('lib/errors/error-handling');
const postService = require('services/server/posts/posts');
const getPostShape = require('lib/shapes/shape-get-posts');

export default async (req, res) => {
  try {
    if (req.method !== 'GET') throw new BadMethodError();
    // eslint-disable-next-line radix
    const postId = parseInt(req.query.postId);
    if (Number.isNaN(postId)) {
      throw new ValidationError('invalid post id', errorCodes.PAYLOAD_MALFORMED, 'post id');
    }

    const post = await postService.getPost(postId, { active: 1 });
    const marshallPost = getPostShape.marshallPostToJson(post);
    return res.status(200).send(marshallPost);
  } catch (error) {
    return errorHandlerApi(error, res);
  }
};
