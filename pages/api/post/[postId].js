import errorCodes from 'lib/errors/error-codes';

const { isUserAuthorised } = require('lib/auth/auth');
const { BadMethodError, ValidationError } = require('lib/errors/errors');
const { errorHandlerApi } = require('lib/errors/error-handling');
const postService = require('services/server/posts/posts');
const getPostShape = require('lib/shapes/shape-get-posts');

export default async (req, res) => {
  // eslint-disable-next-line radix
  const postId = parseInt(req.query.postId);
  if (Number.isNaN(postId)) {
    throw new ValidationError('invalid post id', errorCodes.PAYLOAD_MALFORMED, 'post id');
  }
  try {
    if (req.method === 'GET') {
      isUserAuthorised(req, res);
      const post = await postService.getPost(postId);
      const marshallPost = getPostShape.marshallPostToJson(post);
      return res.status(200).send(marshallPost);
    }

    if (req.method === 'PATCH') {
      isUserAuthorised(req, res);
      await postService.updateExistingPost(postId, req.body);
      return res.status(204).send();
    }
    throw new BadMethodError();
  } catch (error) {
    return errorHandlerApi(error, res);
  }
};
