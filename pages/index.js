import { useRef } from 'react';
import PropTypes from 'prop-types';
import postService from 'services/server/posts/posts';

import Container from 'components/core/layouts/Container';
import SlimContainer from 'components/core/layouts/SlimContainer';
import HomePostColummns from 'components/posts/home-columns/PostsHomeColumns';
import LottieControl from 'components/core/lottie/LotteControl';
import aiRobotData from 'assets/lottie/artificial-intelligence-robot.json';
import randomDots from 'assets/lottie/random-dots-grid.json';
import workspace from 'assets/lottie/work-space.json';
import pattern from 'assets/pattern-min.png';

import dropP from 'assets/drop-p.svg';

import Main from 'components/core/layouts/Main';

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

const Home = ({ posts, randomNumber }) => {
  const parsedPosts = JSON.parse(posts);
  const myRef = useRef(null);
  const executeScroll = () => myRef.current.scrollIntoView();

  const lottieRender = (random) => {
    if ((random % 2) === 1) {
      return (
        <div className="mt-18 mb-22">
          <LottieControl animationData={workspace} width={310} height={240} className="mb-12" />
        </div>
      );
    }
    if (random % 2 === 0) {
      return (
        <div>
          <LottieControl animationData={randomDots} height={200} className="absolute right-8 top-26" />
          <LottieControl animationData={aiRobotData} height={380} />
        </div>
      );
    }
    return (<></>);
  };

  return (
    <>
      <Main>
        <div
          className={`
          bg-contain bg-repeat
          ${(randomNumber % 2 === 0) ? 'pb-9 pt-10  md:py-24' : 'py-20 md:py-36'}
          mb-10
          `}
          style={{ backgroundImage: `url(${pattern})` }}
        >
          <Container>
            <div className="flex flex flex-col justify-center items-center text-center">
              <div className="relative">
                {lottieRender(randomNumber)}
              </div>
              <div className="relative mr-6">
                <button
                  type="button"
                  className="
                    absolute
                    cursor-pointer
                    "
                  onClick={executeScroll}
                  style={{
                    right: '-24px',
                    top: '21px',
                  }}
                >
                  <img
                    src={dropP}
                    alt="p"
                  />
                </button>
                <h2 className="font-bold">
                  {' '}
                  Stephen Whip
                  <span className="hidden">p</span>
                  {' '}
                </h2>
                <p> Web Developer</p>
              </div>
            </div>
          </Container>
        </div>

        <div className="h-12 w-12" ref={myRef} />
        <SlimContainer>
          <HomePostColummns posts={parsedPosts} />
        </SlimContainer>
      </Main>

    </>
  );
};

Home.propTypes = {
  posts: PropTypes.string.isRequired,
  randomNumber: PropTypes.number.isRequired,
};

export default Home;

// This function gets called at build time
export async function getStaticProps() {
  const res = await postService.getAllActivePosts();
  const posts = JSON.stringify(res);
  const randomNumber = Math.floor(getRandomArbitrary(1, 20));
  return {
    props: {
      posts,
      randomNumber,
    },
    revalidate: 10,
  };
}
