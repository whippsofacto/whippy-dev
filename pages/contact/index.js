import { useReducer, useState } from 'react';
import reducers from 'reducers/form-reducer';
import Container from 'components/core/layouts/SlimContainer';
import Main from 'components/core/layouts/Main';
import Header from 'components/core/headers/page-header';
import ContactForm from 'components/forms/contact/';
import CheckButton from 'components/core/buttons/check-button';
import contactService from 'services/client/contact/contact';
import Success from 'components/core/modals/success';
import { ValidationError } from 'lib/errors/errors';

const initialFormState = {
  name: '',
  email: '',
  web: false,
  infra: false,
  seo: false,
  design: false,
  cuppa: false,
  info: '',
};

const initalErrorState = {
  name: null,
  email: null,
  info: '',
};

const Contact = () => {
  const [formState, formDispatch] = useReducer(reducers.formReducer, initialFormState);
  const [errorState, errorDispatch] = useReducer(reducers.formErrorsReducer, initalErrorState);
  const [formEasy, updateFormSelector] = useState(true);
  const [loading, updateLoading] = useState(false);
  const [success, updateSuccess] = useState(false);

  const onSubmit = async (e) => {
    e.preventDefault();
    updateLoading(true);
    try {
      await contactService.postForm(formState);
      updateLoading(false);
      return updateSuccess(true);
    } catch (err) {
      updateLoading(false);
      if (err instanceof ValidationError) {
        if (err.field === 'email') {
          return reducers.createFormErrorsHandler('email', err.message, errorDispatch);
        }
        if (err.field === 'name') {
          return reducers.createFormErrorsHandler('name', err.message, errorDispatch);
        }
      }

      throw new Error(err);
    }
  };
  return (
    <>
      <div className="absolute top-0 right-0 z-10" />
      <Main>
        <Header heading="Contact" />
        <Container>
          <div className="mb-10">
            <div className="hidden flex mb-10">
              <CheckButton label="Easy Mode" className="mr-4" active={formEasy} setActive={() => updateFormSelector(true)} />
              <CheckButton label="Hard Mode" active={!formEasy} setActive={() => updateFormSelector(false)} />
            </div>
            {
            formEasy
            && (
            <ContactForm
              formState={formState}
              formDispatch={formDispatch}
              errorState={errorState}
              initalErrorState={errorState}
              errorDispatch={errorDispatch}
              title="Get In Touch"
              onSubmit={onSubmit}
              loading={loading}
            />
            )
          }
            {
            !formEasy && (
            <p className="mb-4">
              {' '}
              A difficult, time consuming and largely pointless
              contact form is the enemy of lead generation.
              but what about those that want to demonstrate just how much they want to reach you?
              Surely you&apos;d look more favourably over a user that had faught
              tooth and nail to get your attention? Right?
              {' '}

            </p>
            )
          }
          </div>
        </Container>
      </Main>
      <Success
        message="
        Thanks very much for you message. I'll get back you shortly."
        active={success}
        cta="Ok"
        onClick={() => {
          reducers.setFormState(initialFormState, formDispatch);
          updateSuccess(false);
        }}
      />
    </>
  );
};

export default Contact;
