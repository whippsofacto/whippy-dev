import Main from 'components/core/layouts/Main';
import Container from 'components/core/layouts/SlimContainer';
import Header from 'components/core/headers/page-header';
import ExternalLink from 'components/core/links/external-link';

const links = [
  {
    title: 'Sites',
    links: [
      {
        linkText: 'Swiss Army Man',
        link: 'https://swissarmyman.com/',
      }, {
        linkText: 'Supremo',
        link: 'https://www.supremo.co.uk/',
      },
      {
        linkText: 'Akqa',
        link: 'https://akqa.com',
      },

    ],
  },
  {
    title: 'Libs',
    links: [
      {
        linkText: 'A-Frame - web VR',
        link: 'https://aframe.io/',
      },
      {
        linkText: 'Sticky Bits, lightweight alternative to "sticky" polyfills.',
        link: 'https://dollarshaveclub.github.io/stickybits/',
      },
      {
        linkText: 'Tailwind',
        link: 'https://tailwindcss.com/docs',
      },
    ],
  },
  {
    title: 'Articles',
    links: [
      {
        linkText: 'Why You probably dont need moment js anymore.',
        link: 'https://dockyard.com/blog/2020/02/14/you-probably-don-t-need-moment-js-anymore',
      },
      {
        linkText: 'Who The F**K are Studio Something?',
        link: 'https://medium.com/what-the-f-k-is-studio-something/who-the-f-k-are-studio-something-66eed959f666',
      },
    ],
  },
  {
    title: 'Guides',
    links: [{
      linkText: 'Simple Auth with Magic.link and Next.js',
      link: 'https://vercel.com/blog/simple-auth-with-magic-link-and-nextjs',
    }, {
      linkText: 'Markdown Cheatsheet',
      link: 'https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet',
    }],
  },
  {
    title: 'Tools',
    links: [{
      linkText: 'Stack Edit - online markdown editor',
      link: 'https://stackedit.io/app#',
    }],
  },
  {
    title: 'APIs',
    links: [{
      linkText: 'NASA',
      link: 'https://api.nasa.gov/#getting-started',
    }, {
      linkText: 'Gitlab',
      link: 'https://docs.gitlab.com/ee/api/api_resources.html',
    }],
  },
];

const Bookmarks = () => (
  <Main>
    <Header
      heading="Bookmarks"
    />
    <Container>
      <div className="mb-10 lg:grid grid-rows-3 grid-flow-col gap-4 ">
        {
          links.map((category) => (
            <div key={category.title} className="mb-5">
              <h5 className="mb-3">
                {category.title}
              </h5>
              {
                      category.links.map((linkData) => (
                        <div className="mb-2">
                          <ExternalLink
                            key={linkData.link}
                            link={linkData.link}
                            linkText={linkData.linkText}
                          />
                        </div>
                      ))
                  }
            </div>
          ))
      }
      </div>
    </Container>
  </Main>
);
export default Bookmarks;
