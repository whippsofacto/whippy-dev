import PropTypes from 'prop-types';
import Container from 'components/core/layouts/Container';
import SlimContainer from 'components/core/layouts/SlimContainer';
import postService from 'services/server/posts/posts';
import SafeHtml from 'components/core/safe-html/SafeHtml';
import Image from 'components/core/image/Image';
import utils from 'lib/utils/utils';
import fallback from 'public/fallback-post-image-min.png';
import pattern from 'assets/lightning-pattern.png';

import LottieControl from 'components/core/lottie/LotteControl';
import imageLoading from 'assets/lottie/dot-array-multi-coloured.json';
import Main from 'components/core/layouts/Main';

const Post = ({ post }) => {
  const parsed = JSON.parse(post);

  return (
    <Main>
      <div className="py-9">
        <div
          className={`
          bg-contain bg-repeat
          pt-10
          pb-8
          mb-4
          `}
          style={{ backgroundImage: `url(${pattern})` }}
        >
          <Container>
            <div className="relative m-auto max-h-80 sm:max-h-110 w-96 h-96 max-w-full flex items-center">
              <LottieControl animationData={imageLoading} className="absolute md:-right-24 bg-white" />
              <Image className="object-cover rounded w-full h-full relative z-10" src={(parsed.featuredImage) ? `/api/media/${parsed.featuredImage}` : fallback} alt={parsed.title} />
              <LottieControl animationData={imageLoading} className="absolute md:-left-24 bg-white" />
            </div>

            <h1 className="m-auto text-center py-3">{parsed.title}</h1>
            <p className="text-center">
              Date Created:
              {' '}
              <span className="font-bold">
                {utils.uiDate(parsed.dateCreated)}
              </span>
            </p>
            <p className="mb-3 text-center">
              Last Updated:
              {' '}
              <span className="font-bold">
                {`${utils.uiTime(parsed.lastUpdated)}`}
                ,
                {' '}
                {utils.uiDate(parsed.lastUpdated)}
              </span>
            </p>
          </Container>
        </div>
        <SlimContainer>

          <SafeHtml html={parsed.content} />
        </SlimContainer>
      </div>
    </Main>
  );
};

export default Post;

Post.propTypes = {
  post: PropTypes.string.isRequired,
};

// This function gets called at build time
export async function getStaticPaths() {
  // Call an external API endpoint to get posts
  const res = await postService.getAllActivePosts();
  const posts = res;

  // Get the paths we want to pre-render based on posts
  const paths = posts.map((post) => ({
    params: { postId: post.id.toString() },
  }));

  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return { paths, fallback: false };
}

// This function gets called at build time
export async function getStaticProps({ params }) {
  // Call an external API endpoint to get posts
  const res = await postService.getPost(params.postId);
  const post = JSON.stringify(res);
  // By returning { props: { posts } }, the Blog component
  // will receive `posts` as a prop at build time
  return {
    props: {
      post,
    },
  };
}
