import Cms from 'components/core/cms/cms-nav-hoc';
import { useAuth } from 'context/auth';
import getServerSideAuth from 'auth/getServerSideAuth';

export default function Dashboard() {
  const { user } = useAuth();

  return (
    <Cms>
      <h3>
        Dashboard
      </h3>
      <h6>
        Hello,
        {' '}
        {user && user.username}
      </h6>
    </Cms>

  );
}

export async function getServerSideProps(ctx) {
  getServerSideAuth.checkAuth(ctx);
  return { props: {} };
}
