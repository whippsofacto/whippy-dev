import { useEffect, useState } from 'react';
import Cms from 'components/core/cms/cms-nav-hoc';
import InternalLink from 'components/core/links/internal-link';
import Button from 'components/core/buttons/button';
import postService from 'services/client/posts/posts';
import getServerSideAuth from 'auth/getServerSideAuth';

const Posts = () => {
  const [posts, updatePosts] = useState([]);

  useEffect(() => {
    const getPosts = async () => {
      const postData = await postService.getPosts({ all: true });
      updatePosts(postData.results);
    };

    getPosts();
  }, []);

  return (
    <Cms>
      <h3> Posts </h3>
      <hr className="mb-6" />

      <div className="mb-9">
        {
           posts.map((post) => (
             <InternalLink key={post.id} href={`/cms/posts/${post.id}`}>
               <h5>
                 {post.title}
               </h5>
               <hr />
             </InternalLink>
           ))
         }
      </div>

      <InternalLink href="/cms/posts/new" size="sm" className="ml-auto">
        <Button text="Create New" theme="action" />
      </InternalLink>

    </Cms>
  );
};

export default Posts;

export async function getServerSideProps(ctx) {
  getServerSideAuth.checkAuth(ctx);
  return { props: {} };
}
