import { useReducer, useState } from 'react';
import reducers from 'reducers/form-reducer';
import Cms from 'components/core/cms/cms-nav-hoc';
import PostForm from 'components/forms/posts';
import { useRouter } from 'next/router';
// import { AuthorizationError, ValidationError, ServerError } from 'lib/errors/errors';
import postsService from 'services/client/posts/posts';
import SuccessModal from 'components/core/modals/success';
import getServerSideAuth from 'auth/getServerSideAuth';

const initialFormState = {
  title: '',
  excerpt: '',
  content: '',
};

const initalErrorState = {
  title: null,
  excerpt: null,
  content: null,
};

const Posts = () => {
  const [formState, formDispatch] = useReducer(reducers.formReducer, initialFormState);
  const [errorState, errorDispatch] = useReducer(reducers.formErrorsReducer, initalErrorState);
  const [showModal, updateShowModal] = useState(false);
  const [createdPostId, updateCratedPostId] = useState(null);
  const [formLoading, updateFormLoading] = useState(false);
  const router = useRouter();
  const onSubmit = async (e) => {
    e.preventDefault();
    // reducers.createFormErrorsHandler('title', 'Broken', errorDispatch);
    updateFormLoading(true);
    try {
      const createPost = await postsService.createPost(formState);
      updateCratedPostId(createPost.id);
      updateShowModal(true);
    } catch (error) {
      console.log(error);
    }
    updateFormLoading(false);
  };

  return (
    <Cms>
      <PostForm
        formState={formState}
        formDispatch={formDispatch}
        errorState={errorState}
        initalErrorState={errorState}
        errorDispatch={errorDispatch}
        title="New Post"
        onSubmit={onSubmit}
        loading={formLoading}
      />
      <SuccessModal
        message="Post Saved"
        active={showModal}
        cta="Ok"
        onClick={() => router.push(`/cms/posts/${createdPostId}`)}
      />
    </Cms>

  );
};

export default Posts;

export async function getServerSideProps(ctx) {
  getServerSideAuth.checkAuth(ctx);
  return { props: {} };
}
