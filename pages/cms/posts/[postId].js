import { useEffect, useState, useReducer } from 'react';
import { useRouter } from 'next/router';
import Cms from 'components/core/cms/cms-nav-hoc';
import PostForm from 'components/forms/posts';
import reducers from 'reducers/form-reducer';
import postService from 'services/client/posts/posts';
import SuccessModal from 'components/core/modals/success';
import getServerSideAuth from 'auth/getServerSideAuth';

const Post = () => {
  const router = useRouter();
  const { postId } = router.query;
  const [post, upatePost] = useState({});
  const [formState, formDispatch] = useReducer(reducers.formReducer, {});
  const [errorState, errorDispatch] = useReducer(reducers.formErrorsReducer, {});
  const [loading, upateLoading] = useState(true);
  const [showModal, updateShowModal] = useState(false);
  const [formLoading, updateFormLoading] = useState(false);

  useEffect(() => {
    const getPosts = async () => {
      const postData = await postService.getPost(postId);
      reducers.setFormState({
        title: postData.title,
        excerpt: postData.excerpt,
        content: postData.content,
        featuredImage: postData.featuredImage,
        active: postData.active,
      }, formDispatch);
      upatePost(postData);
      upateLoading(false);
    };
    getPosts();
  }, []);

  const onSubmit = async (e) => {
    e.preventDefault();
    updateFormLoading(true);
    // reducers.createFormErrorsHandler('title', 'Broken', errorDispatch);
    try {
      await postService.updatePost(formState, postId);
      updateShowModal(true);
      upatePost({ ...post, title: formState.title });
    } catch (error) {
      console.log(error);
    }
    updateFormLoading(false);
  };
  return (
    <Cms>

      {
        !loading
           && (

           <>
             <PostForm
               formState={formState}
               formDispatch={formDispatch}
               errorState={errorState}
               initalErrorState={errorState}
               errorDispatch={errorDispatch}
               title={post.title}
               onSubmit={onSubmit}
               loading={formLoading}
             />
             <SuccessModal
               message="Post Updated"
               active={showModal}
               cta="Ok"
               onClick={() => updateShowModal(false)}
             />
           </>
           )
      }

    </Cms>
  );
};
export default Post;

export async function getServerSideProps(ctx) {
  getServerSideAuth.checkAuth(ctx);
  return { props: {} };
}
