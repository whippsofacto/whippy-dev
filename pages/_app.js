/* eslint react/jsx-props-no-spreading: 0 */
import PropTypes from 'prop-types';
import 'tailwindcss/tailwind.css';
import '@fontsource/jost';
import '@fontsource/jost/600.css';
import '@fontsource/jost/700.css';
import { AuthProvider } from 'context/auth';

import Head from 'next/head';

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Stephen Whipp </title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
        <script src="/js/tinymce/tinymce.min.js" />

      </Head>
      <AuthProvider>
        <Component {...pageProps} />
      </AuthProvider>
    </>
  );
}

MyApp.defaultProps = {
  pageProps: {},
};

MyApp.propTypes = {
  Component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]).isRequired,
  pageProps: PropTypes.shape({}),
};
export default MyApp;
