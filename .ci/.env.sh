#!/usr/bin/env bash
echo "Creating .env file"
echo "DB_HOST=$DB_HOST" >> .env
echo "DB_NAME=$DB_NAME" >> .env
echo "DB_USER=$DB_USER" >> .env
echo "DB_PASSWORD=$DB_PASSWORD" >> .env
echo "SENDGRID_API_KEY=$SENDGRID_API_KEY" >> .env
echo "AWS_SERVICE_ACCESS_KEY_ID=key-from-aws" >> .env
echo "AWS_SERVICE_SECRET_ACCESS_KEY=key-from-aws" >> .env
echo "S3_BUCKET_NAME=s3-name" >> .env