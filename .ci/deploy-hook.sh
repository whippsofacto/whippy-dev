#!/usr/bin/env bash
echo "Installing curl and calling deploy hook"
echo $CI_JOB_STAGE
apk update && apk add curl
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET $VERCEL_DEPLOY_URL