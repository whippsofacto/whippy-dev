// tailwind.config.js
const plugin = require('tailwindcss/plugin');

const myTheme = {
  colors: {
    primary: '#91e4d7',
  },
  fontSize: {
    'cms-input': '0.975rem',
  },
  listStyleType: {
    none: 'none',
    disc: 'disc',
    decimal: 'decimal',
    square: 'square',
    roman: 'upper-roman',
  },
};

module.exports = {
  purge: ['./pages/**/*.{js,jsx}', './components/**/*.{js,jsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    backgroundColor: (twTheme) => ({
      ...twTheme('colors'),
      primary: myTheme.colors.primary,
    }),
    textColor: (twTheme) => ({
      ...twTheme('colors'),
      primary: myTheme.colors.primary,
    }),
    borderColor: (twTheme) => ({
      ...twTheme('colors'),
      primary: myTheme.colors.primary,
    }),
    fontFamily: {
      sans: ['ui-sans-serif', 'system-ui', 'sans-serif'],
      body: ['jost', 'Helvetica', 'Arial', 'sans-serif'],
    },
    container: {
      center: true,
    },
    extend: {
      zIndex: {
        '-10': '-10',
      },
      fontSize: {
        'cms-input': myTheme.fontSize['cms-input'],
      },
      padding: {
        90: '22rem',
      },
      width: {
        100: '30rem',
        110: '40rem',
      },
      height: {
        100: '30rem',
        110: '40rem',
      },
      maxHeight: {
        110: '40rem',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    plugin(({ addBase, theme }) => {
      // console.log(theme("backgroundColor"))
      addBase({
        h1: {
          fontSize: theme('fontSize.5xl'),
          fontWeight: theme('fontWeight.semibold'),
        },
        h2: {
          fontSize: theme('fontSize.4xl'),
          fontWeight: theme('fontWeight.semibold'),
        },
        h3: {
          fontSize: theme('fontSize.3xl'),
          fontWeight: theme('fontWeight.semibold'),
        },
        h4: {
          fontSize: theme('fontSize.2xl'),
          fontWeight: theme('fontWeight.semibold'),
        },
        h5: {
          fontSize: theme('fontSize.xl'),
          fontWeight: theme('fontWeight.semibold'),
          backgroundColor: theme('colors.pink'),
        },
        h6: {
          fontSize: theme('fontSize.lg'),
          fontWeight: theme('fontWeight.semibold'),
        },
        code: {
          backgroundColor: theme('backgroundColor.gray.50'),
          padding: theme('padding.4'),
          display: 'block',
        },
      });
    }),
  ],
};
