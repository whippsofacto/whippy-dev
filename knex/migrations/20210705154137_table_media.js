exports.up = (knex) => knex.schema.createTable('media', (table) => {
  table.increments();
  table.string('name').notNullable();
  table.string('fileName').notNullable();
});

exports.down = (knex) => knex.schema.dropTable('media');
