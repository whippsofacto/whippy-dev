exports.up = (knex) => knex.schema.createTable('posts', (table) => {
  table.increments();
  table.string('title').notNullable();
  table.text('content');
  table.string('excerpt', 500);
  table.timestamp('dateCreated').notNullable().defaultTo(knex.fn.now());
  table.timestamp('lastUpdated').notNullable().defaultTo(knex.fn.now());
  table.integer('createdBy').notNullable();
  table.integer('featuredImage');
});

exports.down = (knex) => knex.schema.dropTable('posts');
