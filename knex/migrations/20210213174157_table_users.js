exports.up = (knex) => knex.schema.createTable('users', (table) => {
  table.increments();
  table.string('email', 254);
  table.unique('email');
  table.string('username');
});

exports.down = (knex) => knex.schema.dropTable('users');
