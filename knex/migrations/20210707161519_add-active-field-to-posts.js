exports.up = (knex) => knex.schema.table('posts', (table) => table.boolean('active')
  .notNullable()
  .defaultTo(true));

exports.down = () => {};
