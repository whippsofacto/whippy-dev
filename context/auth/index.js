import {
  createContext, useState, useContext, useEffect,
} from 'react';
import CONSTS from 'lib/conts/system';

import PropTypes from 'prop-types';
import Cookies from 'js-cookie';
import { useRouter } from 'next/router';

const AuthContext = createContext({});

const storeUserInLocalStorage = (userData) => {
  if (!userData) {
    localStorage.removeItem('user');
    return;
  }

  localStorage.setItem('user', JSON.stringify(userData));
};

const getUserFromLocalStorage = () => {
  const user = localStorage.getItem('user');

  if (user) {
    return JSON.parse(user);
  }

  return undefined;
};

export const AuthProvider = ({ children }) => {
  const router = useRouter();
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState(null);

  useEffect(() => {
    setUser(getUserFromLocalStorage());
  }, [router.pathname]);

  const logout = (options = {}) => {
    Cookies.remove(CONSTS.COOKIE_TOKEN);
    if (options['404']) {
      router.push('/404');
    } else {
      router.push(CONSTS.REDIRECT_ON_FAIL_DEST);
    }
  };

  // on valid login, we'll clear out any old stuff and replace with new stuff
  const onValidLogin = async (token, userData) => {
    // Clear up any existing sessions before attempting to login
    storeUserInLocalStorage(undefined);
    await Cookies.remove(CONSTS.COOKIE_TOKEN);

    Cookies.set(CONSTS.COOKIE_TOKEN, token);
    storeUserInLocalStorage(userData);
    return userData;
  };

  return (
    <AuthContext.Provider value={{
      loading,
      onValidLogin,
      logout,
      setLoading,
      getUserFromLocalStorage,
      user,
    }}
    >
      {children}
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useAuth = () => useContext(AuthContext);

export const ProtectRoute = ({ children }) => {
  const router = useRouter();
  const {
    logout, user, loading, setLoading,
  } = useAuth();

  useEffect(() => {
    const isPrivateRoute = router.pathname.includes(CONSTS.PROTECTED_PATH);
    if (!user && isPrivateRoute) {
      logout({ 404: true });
    } else {
      setLoading(false);
    }
  }, [router.pathname]);

  if (!loading) {
    return children;
  }

  return '';
};
