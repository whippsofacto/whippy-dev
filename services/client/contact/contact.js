import api from 'lib/http/http';

/**
 * Contact
 *
 * Post contact form to to the contact endpoint.
 * @param {formData} object - Payload from the contact form page.
 * @return {Promise<array[]>}
 */
const postForm = (formData) => api('post', 'contact/me', { formData });

const contact = {
  postForm,
};

export default contact;
