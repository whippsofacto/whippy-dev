import api from 'lib/http/http';

/**
 * Login
 *
 * Post an email address to the login endpoint.
 * @param {string} email - Email address string
 * @return {Promise<array[]>}
 */
const login = (email) => api('post', 'auth/login', { email });

const verifyToken = (token) => api('post', 'auth/verify', { token });

const auth = {
  login,
  verifyToken,

};

export default auth;
