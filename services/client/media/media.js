import api from 'lib/http/http';

const { BadGateway } = require('lib/errors/errors');

/**
 * Upload Media
 *
 * Post files to the media endpoint
 * @param {blob} files - file upload
 * @return {Promise<array[]>}
 */
const upload = (formData) => api('post', 'media', formData);
const richTextMediaUploadHandler = async (blobInfo, success, failure) => {
  const formData = new FormData();
  formData.append('file', blobInfo.blob(), blobInfo.filename());
  try {
    const resp = await upload(formData);
    const { id } = resp;
    success(`/api/media/${id}`);
  } catch (error) {
    if (error instanceof BadGateway) {
      failure(`Code: ${error.code}, Bad Gateway.`);
    } else {
      failure('Something went wrong!');
    }
  }
};

// eslint-disable-next-line consistent-return
const postMedia = async (file) => {
  if (file) {
    const formData = new FormData();
    formData.append('file', file[0], file[0].name);
    try {
      const resp = await upload(formData);
      return resp.id;
    } catch (error) {
      if (error instanceof BadGateway) {
        return `Code: ${error.code}, Bad Gateway.`;
      }
      return 'Something went wrong!';
    }
  }
};

const media = {
  upload,
  richTextMediaUploadHandler,
  postMedia,
};

export default media;
