import api from 'lib/http/http';

/**
 * createPost
 *
 * create a new post
 * @param {Object} post - {}
 * @return {Promise<object{}>}
 */
const createPost = (post) => api('post', 'posts', post);

/**
 * getPosts
 *
 * create a new post
 * @param {Object} post - {}
 * @return {Promise<object{}>}
 */
const getPosts = (queryParams = {}) => {
  // allowed params
  //  limit: null
  //  start: 0
  //  results: []

  const queryStrings = [];
  if (queryParams.limit) {
    queryStrings.push(`limit=${queryParams.limit}`);
  }

  if (queryParams.start) {
    queryStrings.push(`start=${queryParams.start}`);
  }

  let query = '';
  if (queryStrings.length) {
    query = `?${queryStrings.join('&')}`;
  }

  const posts = api('get', `posts${query}`);
  return posts;
};

const getPublicPosts = (queryParams = {}) => {
  const queryStrings = [];
  if (queryParams.limit) {
    queryStrings.push(`limit=${queryParams.limit}`);
  }

  if (queryParams.start) {
    queryStrings.push(`start=${queryParams.start}`);
  }

  let query = '';
  if (queryStrings.length) {
    query = `?${queryStrings.join('&')}`;
  }

  const posts = api('get', `posts/public${query}`);
  return posts;
};

const getPost = (postId) => api('get', `post/${postId}`);
const getPublicPost = (postId) => api('get', `post/public/${postId}`);

const updatePost = (post, postId) => api('patch', `post/${postId}`, post);

module.exports = {
  createPost,
  getPosts,
  getPost,
  getPublicPosts,
  getPublicPost,
  updatePost,
};
