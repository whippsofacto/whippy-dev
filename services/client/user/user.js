import api from 'lib/http/http';

/**
 * getUser
 *
 * Get a user by passing up an id
 * @param {int} id - UserId
 * @return {Promise<object{}>}
 */
const getUser = (id, token = undefined) => api('get', `user/${id}`, {}, token);

/**
 * getUserFromToken
 *
 * @param {string} token - auth token
 * @return {Promise<object{}>}
 */
const getUserFromToken = (token = undefined) => api('get', 'user/token', {}, token);

const user = {
  getUser,
  getUserFromToken,
};

export default user;
