const logger = require('lib/logger/logger');
const { createAppToken } = require('lib/auth/auth');
const { sendLoginEmail } = require('services/server/email-service/email-service');

/**
 * Send E-Mail through sendgrd
 *
 * @param {object} user - The recipent of the email
 * @return {createAndSendUserAuthToken~sendLoginEmail} - function that sends user a login email
 */
const createAndSendUserAuthToken = async (user) => {
  const token = createAppToken({ id: user.id });
  logger.info(`Auth Service - Attempting to send new token to user: ${user.id}`);
  return sendLoginEmail(user.email, token);
};

module.exports = {
  createAndSendUserAuthToken,
};
