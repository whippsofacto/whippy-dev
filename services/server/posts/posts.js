/* eslint-disable radix */
import { db } from 'lib/db/db';
import postConsts from 'lib/conts/post';
import utils from 'lib/utils/utils';
import { ValidationError } from 'lib/errors/errors';
import errorCodes from 'lib/errors/error-codes';
import logger from 'lib/logger/logger';

const insertPost = (payload) => db().insert('posts', payload);
const addToPostObject = (payload, userId, options = {}) => {
  const updatePayload = payload;

  if (options.new) {
    updatePayload.createdBy = userId;
    updatePayload.dateCreated = new Date();
  }

  updatePayload.lastUpdated = new Date();

  return updatePayload;
};

const getPost = (postId, wheres = {}) => db().findOne('posts', { id: postId, ...wheres });
const updatePost = (postId, post) => db().update('posts', postId, post);
const getAllActivePosts = () => db().rawSql('SELECT * from posts WHERE active=true ORDER BY dateCreated DESC');
const getPostsQuery = (params) => {
  const whereParams = [];
  let wheres = '';
  if (params.active) {
    whereParams.push('active=1');
  }

  if (whereParams.length === 1) {
    wheres = `WHERE ${whereParams.join(' ')}`;
  }

  if (whereParams.length > 1) {
    wheres = `WHERE ${whereParams.join(' AND ')}`;
  }

  const query = `
  SELECT * FROM posts
  ${wheres}
  LIMIT ${params.start},${params.limit}
  `;

  return db().rawSql(query);
};

const createNewPost = (postBody, userId) => {
  const additonalKeys = utils.checkKeys(postConsts.ALLOWED_POST_FIELDS, postBody);
  if (additonalKeys.length) {
    throw new ValidationError('', errorCodes.PAYLOAD_MALFORMED, 'keys');
  }

  const post = addToPostObject(postBody, userId, { new: true });

  return insertPost(post);
};

const getPosts = (queryParams) => {
  // check the allowed params
  // limit and start
  const ALLOWED_PARAMS = postConsts.ALLOWED_QUERY_PARAMS;
  let start = 0;
  let limit = 30;
  let active = null;

  if (queryParams[ALLOWED_PARAMS.start]) {
    const startParam = parseInt(queryParams[ALLOWED_PARAMS.start]);
    if (!Number.isNaN(startParam)) {
      start = startParam;
    }
  }

  if (queryParams[ALLOWED_PARAMS.limit]) {
    const limitParam = parseInt(queryParams[ALLOWED_PARAMS.limit]);
    if (!Number.isNaN(limitParam)) {
      limit = limitParam;
    }

    if (start > limit) {
      logger.info(`Start cannot be greater than limit, ${start}, limit:  ${limit}. Will return default`);
      start = 0;
      limit = 30;
    }
  }

  if (queryParams[ALLOWED_PARAMS.active]) {
    const activeParam = queryParams[ALLOWED_PARAMS.active];
    if (activeParam === 'true' || activeParam === true) {
      active = true;
    }
  }

  return getPostsQuery({ start, limit, active });
};

const updateExistingPost = (postId, postBody) => {
  const additonalKeys = utils.checkKeys(postConsts.ALLOWED_POST_FIELDS, postBody);
  if (additonalKeys.length) {
    throw new ValidationError('', errorCodes.PAYLOAD_MALFORMED, 'keys');
  }

  const post = addToPostObject(postBody);
  return updatePost(postId, post);
};

module.exports = {
  createNewPost,
  getPosts,
  getPost,
  updateExistingPost,
  getAllActivePosts,
};
