// Import required AWS SDK clients and commands for Node.js.
import { PutObjectCommand, GetObjectCommand } from '@aws-sdk/client-s3';
import logger from 'lib/logger/logger';
import S3 from 'lib/s3/s3';

const { db } = require('lib/db/db');
const FileType = require('file-type');
const { BadGateway } = require('lib/errors/errors');
const errorCodes = require('lib/errors/error-codes');

const fs = require('fs');

const uploadMediaToS3 = async (file, folder) => {
  const readFile = fs.readFileSync(file.path);
  const { ext } = await FileType.fromFile(file.path);
  const fileName = `${file.filename}.${ext}`;
  const path = `${folder}/${fileName}`;

  // Set the parameters
  const uploadParams = {
    Bucket: process.env.S3_BUCKET_NAME,
    Key: path,
    Body: readFile,
  };

  try {
    await S3.s3Client.send(new PutObjectCommand(uploadParams));
  } catch (error) {
    logger.error(`Problem uploading to S3: ${error}`);
    throw new BadGateway('S3 UPLOAD', errorCodes.UPLOADING_TO_S3_ERROR, 'S3 Upload Failed');
  }

  const insertId = await db().insert('media', {
    name: file.originalname,
    fileName: path,
  });

  // delete the temp file
  fs.unlinkSync(file.path);

  return { id: insertId };
};

const getMediaFromS3 = async (mediaId) => {
  const fileRecord = await db().findOne('media', { id: mediaId });

  const bucketParams = {
    Bucket: process.env.S3_BUCKET_NAME,
    Key: fileRecord.fileName,
  };

  // Get the object} from the Amazon S3 bucket. It is returned as a ReadableStream.
  const data = await S3.s3Client.send(new GetObjectCommand(bucketParams));

  return data.Body;
};

module.exports = {
  uploadMediaToS3,
  getMediaFromS3,
};
