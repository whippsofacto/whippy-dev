const { db } = require('lib/db/db');

const getUserById = (id) => db().findOne('users', { id });
const getUserByEmail = (email) => db().findOne('users', { email });

module.exports = {
  getUserById,
  getUserByEmail,
};
