const { SIGN_IN_TEMPLATE, CONTACT_FORM_TEMPLATE } = require('lib/email/templates');
const { sendEmail } = require('lib/email/email');
const logger = require('lib/logger/logger');

/**
 * sendLoginEmail - takes in an emall address and a token, passes that to
 * the sendMail lib function which it then returns
 *
 * @param {string} toEmail - email address to send to
 * @param {string} token - Sender email address, will default to 'tom-tom-beep-boop[at]whipp.dev'
 * @return {sendLoginEmail~sendEmail} - returns the sendMail function
 */
const sendLoginEmail = (toEmail, token) => {
  const ctaLink = `${process.env.DOMAIN}/login/token-verification?token=${token}`;
  const template = SIGN_IN_TEMPLATE(ctaLink);

  return sendEmail(toEmail, undefined, 'Sign In', template);
};

const sendContactEmail = (formData) => {
  const template = CONTACT_FORM_TEMPLATE(formData);
  logger.info('Attempting to send a Contact Form email.');
  return sendEmail('stephen@whipp.dev', undefined, 'Big Strong Enquiry', template);
};

module.exports = {
  sendLoginEmail,
  sendContactEmail,
};
