const { sendContactEmail } = require('services/server/email-service/email-service');

const createContactEmail = async (formData) => sendContactEmail(formData);

module.exports = {
  createContactEmail,
};
