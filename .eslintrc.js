module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  settings: {
    // set our impoort pth to the root
    'import/resolver': {
      node: {
        paths: ['.'],
      },
    },
  },
  ignorePatterns: ['public/**/*.js'],
  rules: {
    // suppress errors for missing 'import React' in files
    'react/react-in-jsx-scope': 'off',
    // allow jsx syntax in js files (for next.js project)
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'jsx-a11y/label-has-associated-control': [2, {
      // allow htmlFor as associated control or wrap input in label
      assert: 'either',
      // can only be 1 JSX element deep
      depth: 1,
    }],
  },
};
