const sgMail = require('@sendgrid/mail');
const logger = require('lib/logger/logger');
const { ServerError } = require('lib/errors/errors');
const errorCodes = require('lib/errors/error-codes');

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

/**
 * Send E-Mail through sendgrd
 *
 * @param {string} to - The recipent of the email
 * @param {string} from - Sender email address, will default to 'tom-tom-beep-boop[at]whipp.dev'
 * @param {string} subject - Email subject
 * @param {object} template - {text, html}
 * @return {sendEmail~sgMail} - sendgrd mail function
 */
const sendEmail = (to, from, subject, template) => {
  const msg = {
    to,
    from: {
      email: from || 'tom-tom-beep-boop@whipp.dev',
      name: 'Whippy',
    },
    subject,
    text: template.text,
    html: template.html,
  };

  // TODO: have this file chuck an error that can be stored as a log
  return sgMail
    .send(msg)
    .then(() => {},
      (error) => {
        if (error.response) {
          logger.error(error.response.body);
        }
        logger.error("looks like Sendgrid errored and didn't return a response");
        throw new ServerError(errorCodes.SENDGRID_ERROR);
      });
};

module.exports = {
  sendEmail,
};
