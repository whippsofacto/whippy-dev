const ifTrueReturnString = (value, string) => {
  if (value) {
    return string;
  }
  return '';
};

const SIGN_IN_TEMPLATE = (ctaLink) => {
  const html = `
    <div>
        <h1> Sign In  </h1>
        <p> You can login using the sign in link below </p>
        <a href="${ctaLink}"> Sign In </a>
    </div>
    `;
  const text = 'Login, yuo can login usng the link bellow';

  return {
    html, text,
  };
};

const CONTACT_FORM_TEMPLATE = (formData) => {
  const html = `
    <div>
        <h3> You have a new enquiry! </h3>
        <p> Email: <strong> ${formData.email} </strong></p>
        <p> Name: <strong> ${formData.name} </strong></p>
        <p> Additional Info: <strong>${formData.info}</strong></p>
        <h4> Service Selections </h4>
        ${ifTrueReturnString(formData.web, '  <p> Web </p>')}
        ${ifTrueReturnString(formData.infra, '  <p> Infra </p>')}
        ${ifTrueReturnString(formData.seo, '  <p> SEO </p>')}
        ${ifTrueReturnString(formData.design, '  <p> Design </p>')}
        ${ifTrueReturnString(formData.cuppa, '  <p> Cuppa </p>')}
    </div>
    `;
  const text = 'You have a new enquiry!';

  return {
    html, text,
  };
};

module.exports = {
  SIGN_IN_TEMPLATE,
  CONTACT_FORM_TEMPLATE,
};
