const marshallUserToJson = (user) => ({
  username: user.username,
  email: user.email,
});

module.exports = {
  marshallUserToJson,
};
