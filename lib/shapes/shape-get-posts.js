const marshallPagedPostToJson = (posts = []) => ({
  total: posts.length,
  results: posts,
});

const marshallPostToJson = (post) => post;

module.exports = {
  marshallPagedPostToJson,
  marshallPostToJson,
};
