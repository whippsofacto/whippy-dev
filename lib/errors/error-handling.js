const {
  ValidationError, AuthorizationError, BadMethodError, BadGateway,
} = require('lib/errors/errors');
const errorCodes = require('lib/errors/error-codes');
const logger = require('lib/logger/logger');

const errorHandlerApi = (error, res) => {
  logger.error(`errorHandlerApi:  ${error}`);
  if (error instanceof AuthorizationError) {
    return res.status(401).send({ code: error.code, message: error.message });
  }
  if (error instanceof ValidationError) {
    return res.status(422).send({ code: error.code, message: error.message, field: error.field });
  }
  if (error instanceof BadMethodError) {
    return res.status(405).send();
  }

  if (error instanceof BadGateway) {
    return res.status(502).send({ code: error.code });
  }

  return res.status(500).send({ code: errorCodes.SERVER_ERROR, message: 'Something went wrong.' });
};

const parseAppError = (error) => JSON.stringify(error);

module.exports = {
  errorHandlerApi,
  parseAppError,
};
