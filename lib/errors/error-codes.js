module.exports = {
  PAYLOAD_MALFORMED: 100,
  // 400's auth errors
  NO_TOKEN: 401,
  NOT_A_VALID_TOKEN: 402,
  REQUESTED_RESOURCE_COULD_NOT_BE_FOUND: 403,

  // 500's something's just totally broken
  SERVER_ERROR: 500,
  // 700's validation errors
  NOT_A_VALID_EMAIL_ADDRESS: 700,
  NO_EMAIL_ADDRESS_SET: 701,
  NO_NAME_FIELD: 702,
  SHOULD_BE_BOOLEAN_VALUE: 703,
  EMAIL_FIELD_EXCEEDS_MAXIUM_CHARS: 704,
  NAME_FIELD_EXCEEDS_MAXIUM_CHARS: 705,

  // 800's are client side specific
  HTTP_ERROR: 800,

  // 900's are when we are a gateway to another service and it fails
  SENDGRID_ERROR: 900,
  UPLOADING_TO_S3_ERROR: 901,

};
