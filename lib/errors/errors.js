/* eslint-disable  max-classes-per-file */
const errorCodes = require('lib/errors/error-codes');

class AuthorizationError extends Error {
  constructor(message, code) {
    super(message);
    this.name = 'Unauthorized';
    this.messsage = message;
    this.code = code;
  }
}

class ValidationError extends Error {
  constructor(message, code, field) {
    super(message);
    this.name = 'Validation Error';
    this.field = field;
    this.code = code;
    this.message = message;
  }
}

class BadMethodError extends Error {
  constructor(message) {
    super(message);
    this.name = 'Method Not Allowed ';
  }
}

class ServerError extends Error {
  constructor(code, message) {
    super();
    this.name = 'Server Error';
    this.code = code || errorCodes.SERVER_ERROR;
    this.message = message || 'Something went wrong on the server';
  }
}

class BadGateway extends Error {
  constructor(message, code) {
    super();
    this.name = 'Upstream Service';
    this.code = code;
    this.message = message;
  }
}

class HttpError extends Error {
  constructor(message) {
    super(message);
    this.name = 'HTTP Error';
    this.code = errorCodes.HTTP_ERROR;
    this.message = 'There was an uncaught problem with this request';
  }
}

module.exports = {
  AuthorizationError,
  ValidationError,
  BadMethodError,
  ServerError,
  HttpError,
  BadGateway,
};
