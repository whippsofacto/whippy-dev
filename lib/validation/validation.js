const validator = require('email-validator');

module.exports.validateEmail = (email) => validator.validate(email);
