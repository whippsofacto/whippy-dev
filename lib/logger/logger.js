const winston = require('winston');

const colorizer = winston.format.colorize();
const { combine } = winston.format;
const logger = winston.createLogger({
  level: 'info',
  format: combine(
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    winston.format.simple(),
    winston.format.printf((msg) => `${colorizer.colorize(msg.level, `${msg.level}`)} [${msg.timestamp}] - ${msg.message}`),
  ),
  transports: [
    // - Write all logs to the console
    new winston.transports.Console(),
  ],
});

module.exports = logger;
