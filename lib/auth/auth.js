const jwt = require('jsonwebtoken');
const cookie = require('cookie');
const { AuthorizationError } = require('lib/errors/errors');
const errorCodes = require('lib/errors/error-codes');

/**
 * takes the userData to be encoded and returns a signed jwt
 *
 * @param {object} userData - userData to be encoded, ! should never contain sensetive information !
 * @return {string} token - token
 */
const createAppToken = (userData) => jwt.sign(
  userData, process.env.SALT, { expiresIn: process.env.JWT_EXPIRY_TIME },
);

const verifyJwt = (token) => {
  try {
    jwt.verify(token, process.env.SALT);
    return true;
  } catch (err) {
    return false;
  }
};

const decodeJwt = (token) => jwt.verify(token, process.env.SALT);

const isUserAuthorised = (req) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    throw new AuthorizationError('Unauthorized', errorCodes.NO_TOKEN);
  }

  const token = authHeader.split(' ');
  const validToken = verifyJwt(token[1]);
  if (!validToken) {
    throw new AuthorizationError('Unauthorized', errorCodes.NOT_A_VALID_TOKEN);
  }

  const user = decodeJwt(token[1]);
  req.user = { id: user.id };
};

const getCookieFromReqHeaders = (headerString, cookieName) => {
  const cookies = cookie.parse(headerString);
  return cookies[cookieName];
};

module.exports = {
  createAppToken,
  verifyJwt,
  decodeJwt,
  isUserAuthorised,
  getCookieFromReqHeaders,
};
