const knexInit = require('knex');
const config = require('../../knexfile.js');

const knex = knexInit(config);

module.exports.db = () => {
  const db = {
    find: (table, where) => {
      if (where) {
        return knex(table).where(where);
      }
      return knex(table);
    },
    findOne: async (table, where = {}) => {
      const result = await knex(table).where(where);
      if (result.length > 0) return result[0];
      return [];
    },
    insert: async (table, values = {}) => {
      const insert = await knex(table).insert(values);
      return insert[0];
    },
    rawSql: async (query) => {
      const results = await knex.raw(query);

      let result = [];
      if (results.length) {
        // eslint-disable-next-line prefer-destructuring
        result = results[0];
      }
      return result;
    },
    update: async (table, id, values) => {
      const record = await knex(table)
        .update(values)
        .where({ id });

      if (!record) {
        throw new Error(`Coudn't update ${table}, row ${id}. Somethiing went wrong.`);
      }
    },
  };

  return db;
};
