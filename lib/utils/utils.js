import { v4 as uuidv4 } from 'uuid';

const checkKeys = (allowedKeys, payloadObject = {}) => {
  const dissallowedKeys = [];
  const payloadKeys = Object.keys(payloadObject);

  payloadKeys.forEach((payloadKey) => {
    if (!allowedKeys.includes(payloadKey)) {
      dissallowedKeys.push(payloadKey);
    }
  });

  return dissallowedKeys;
};

const uiDate = (date) => new Date(date).toLocaleDateString(
  'en-gb',
  {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  },
);

const uiTime = (date) => new Date(date).toLocaleTimeString('en-gb', { hour: '2-digit', minute: '2-digit' });
const uuid = uuidv4();

module.exports = {
  checkKeys,
  uiDate,
  uiTime,
  uuid,
};
