module.exports = {
  COOKIE_TOKEN: 'whipp-dev-token',
  REDIRECT_ON_FAIL_DEST: '/login',
  REDIRECT_ON_SUCCESS_DEST: '/cms/dashboard',
  REDIRECT_TO_404: '/404',
  PROTECTED_PATH: '/cms',
};
