const ALLOWED_POST_FIELDS = ['title', 'excerpt', 'content', 'featuredImage', 'active'];
const ALLOWED_QUERY_PARAMS = {
  limit: 'limit',
  start: 'start',
  active: 'active',
};
module.exports = {
  ALLOWED_POST_FIELDS,
  ALLOWED_QUERY_PARAMS,
};
