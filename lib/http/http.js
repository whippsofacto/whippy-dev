import { create } from 'apisauce';
import Cookie from 'js-cookie';
import {
  AuthorizationError, ValidationError, ServerError, HttpError, BadGateway,
} from 'lib/errors/errors';
import CONSTS from 'lib/conts/system';
/**
 * This is a function.
 *
 * @param {string} method - A Http method string e.g. "get', "post"
 * @param {string} endpoint - Path to connect to
 * @param {object} payload - An object of data to pass up
 * @param {string} token - Optionally pass a token for server side requests
 * @return {Promise<object{}>}
 *
 */

const initApi = async (method, endpoint, payload = {}, token) => {
  const headers = {};

  const authCookie = Cookie.get(CONSTS.COOKIE_TOKEN);
  if (authCookie) {
    headers.Authorization = `Bearer: ${authCookie}`;
  } else if (token) {
    headers.Authorization = `Bearer: ${token}`;
  }

  let baseUrl = '/api';

  if (!process.browser) {
    baseUrl = `${process.env.DOMAIN}/api`;
  }

  const api = create({
    baseURL: baseUrl,
    headers,
  });

  // start making calls
  const resp = await api[method](endpoint, payload);

  if (resp.ok) {
    return resp.data;
  }

  if (resp.status === 401) {
    throw new AuthorizationError(resp.data.code, resp.data.message);
  }
  if (resp.status === 422) {
    throw new ValidationError(resp.data.message, resp.data.code, resp.data.field);
  }

  if (resp.status === 502) {
    throw new BadGateway(resp.data.message, resp.data.code);
  }

  if (resp.status === 500) {
    throw new ServerError();
  }

  throw new HttpError();
};

export default initApi;
