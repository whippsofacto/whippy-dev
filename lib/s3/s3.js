import { S3Client } from '@aws-sdk/client-s3';

const REGION = 'eu-west-2';
const AWS_ACCESS_KEY_ID = process.env.AWS_SERVICE_ACCESS_KEY_ID;
const AWS_SECRET_ACCESS_KEY = process.env.AWS_SERVICE_SECRET_ACCESS_KEY;

// access credentials are automagically sniffed from .env
// https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/loading-node-credentials-environment.html
// however, then I found this: https://vercel.com/docs/platform/limits?query=reserve#reserved-variables
const s3Client = new S3Client({
  region: REGION,
  credentials: {
    accessKeyId: AWS_ACCESS_KEY_ID,
    secretAccessKey: AWS_SECRET_ACCESS_KEY,
  },
});
module.exports = {
  s3Client,
};
