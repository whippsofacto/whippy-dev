import { v4 as uuidv4 } from 'uuid';

const multer = require('multer');

// will store the image in the temp folder, this is created by multer.
const storage = multer.diskStorage({
  filename(req, file, cb) {
    cb(null, `${uuidv4()}-${Date.now()}`);
  },
});

const limits = {
  // max file size 2mb
  fileSize: 2000000,
};

const upload = multer({ storage, limits });
const uploadSingle = upload.single('file');

module.exports = {
  upload,
  uploadSingle,
};
