require('dotenv').config();

module.exports = {
  client: 'mysql',
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    typeCast(field, next) {
      if (field.type === 'TINY') {
        return (field.string() === '1'); // 1 = true, 0 = false
      }
      return next();
    },
  },
  migrations: {
    directory: `${__dirname}/knex/migrations`,
  },
  seeds: {
    directory: `${__dirname}/knex/seeds`,
  },

};
