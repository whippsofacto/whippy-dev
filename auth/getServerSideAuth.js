import CONSTS from 'lib/conts/system';
import auth from 'lib/auth/auth';

// eslint-disable-next-line consistent-return
const checkAuth = (ctx) => {
  // init token var
  let token;

  // if there's cookies
  if (ctx.req.headers.cookie) {
    // lets extrct cookie
    token = auth.getCookieFromReqHeaders(
      ctx.req.headers.cookie, CONSTS.COOKIE_TOKEN,
    );
  }

  // if there's no coookie
  // redirect away
  if (!token) {
    ctx.res.writeHead(302, { Location: CONSTS.REDIRECT_TO_404 });
    return ctx.res.end();
  }

  const validToken = auth.verifyJwt(token);
  if (!validToken) {
    ctx.res.writeHead(302, { Location: CONSTS.REDIRECT_TO_404 });
    return ctx.res.end();
  }
};

module.exports = {
  checkAuth,
};
