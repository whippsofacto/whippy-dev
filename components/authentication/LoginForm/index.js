import { AuthorizationError, ValidationError, ServerError } from 'lib/errors/errors';
import Success from 'components/core/modals/success';
import { useState } from 'react';
import auth from 'services/client/auth/auth';

const LoginForm = () => {
  const [error, updateError] = useState(null);
  const [active, updateActive] = useState(false);
  const [loading, updateLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const email = e.target.email.value;
    if (!email) {
      updateError('please provide a valid email address');
      return;
    }

    try {
      updateLoading(true);
      await auth.login(email);
      updateActive(true);
      updateLoading(false);
    } catch (err) {
      updateLoading(false);
      if (err instanceof AuthorizationError) {
        updateError(err.message);
        return;
      }
      if (err instanceof ValidationError) {
        updateError(err.message);
        return;
      }
      if (err instanceof ServerError) {
        updateError(err.message);
        return;
      }

      throw new Error(err);
    }
  };

  const clearError = () => {
    if (error) {
      updateError(null);
    }
  };

  return (
    <>
      <section className="container">
        <div className="bg-white xl:w-4/12 lg:w-6/12 md:6/12 w-10/12 m-auto my-10 shadow-md p-6">
          <h3> Sign In </h3>
          <form onSubmit={handleSubmit}>
            <div className="my-5 text-sm">
              <label htmlFor="email" className="block text-black font-semibold mb-3">Email</label>
              <input
                type="email"
                id="email"
                name="email"
                onChange={clearError}
                className={
              `rounded-sm px-4 py-3 mb-1 focus:outline-none bg-gray-100 w-full
               ${error && 'bg-red-100'}
              `
              }
                placeholder="email"
              />
              <span className="mb-2 text-red-400">
                { error && error}
              </span>
            </div>
            <button
              disabled={!!(loading)}
              type="submit"
              className={`
                    ${(loading && 'cursor-not-allowed')}
                    flex justify-center bg-gray-800 text-white py-2 w-full
                    px-3 rounded hover:bg-gray-700 focus:outline-none focus:bg-gray-700"
                    `}
            >
              {
                loading && (
                <svg className="animate-spin -ml-1 mr-3 h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                  <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4" />
                  <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z" />
                </svg>
                )
              }
              Submit
            </button>
          </form>
        </div>
      </section>
      <Success
        message="Please check your emails.
        If you see your e-mail provider below, please click and we'll redirect you in the browser."
        template="toEmail"
        active={active}
      />
    </>
  );
};

export default LoginForm;
