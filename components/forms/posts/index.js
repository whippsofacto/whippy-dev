import PropTypes from 'prop-types';
import reducers from 'reducers/form-reducer';
import InputField from 'components/core/forms/input-field';
import TextArea from 'components/core/forms/textarea-field';
import RichTextField from 'components/core/forms/rich-text-field';
import Button from 'components/core/buttons/button';
import MediaUploader from 'components/core/uploaders/media-uploader/MediaUploader';
import Switch from 'components/core/forms/switch/Switch';

const PostForm = ({
  formState,
  errorState,
  formDispatch,
  errorDispatch,
  onSubmit,
  title,
  loading,
}) => (

  <form onSubmit={(e) => onSubmit(e)}>
    <h3>
      {title}
    </h3>
    <MediaUploader
      id="featuredImage"
      value={formState.featuredImage}
      onChange={(id) => {
        reducers.formInputHandler(id, formDispatch, { fieldName: 'featuredImage', valueNotEvent: true });
        reducers.clearFieldError('featuredImage', errorState, errorDispatch);
      }}
    />
    <Switch
      id="active"
      value={formState.active}
      onChange={(active) => {
        reducers.formInputHandler(active, formDispatch, { fieldName: 'active', valueNotEvent: true });
        reducers.clearFieldError('featuredImage', errorState, errorDispatch);
      }}
    />
    <InputField
      label="Title"
      name="title"
      placeholder="Title..."
      maxlength={255}
      value={formState.title}
      error={errorState.title}
      required
      onChange={(e) => {
        reducers.formInputHandler(e, formDispatch);
        reducers.clearFieldError('title', errorState, errorDispatch);
      }}
    />
    <TextArea
      label="Excerpt"
      name="excerpt"
      placeholder="Excerpt..."
      type="textarea"
      rows={4}
      maxlength={500}
      value={formState.excerpt}
      error={errorState.excerpt}
      onChange={(e) => {
        reducers.formInputHandler(e, formDispatch);
        reducers.clearFieldError('excerpt', errorState, errorDispatch);
      }}
    />
    <RichTextField
      id="content"
      label="Content"
      name="content"
      placeholder="Add content here..."
      value={formState.content}
      error={errorState.content}
      onChange={(e) => {
        reducers.formInputHandler(e, formDispatch, { fieldName: 'content', richText: true });
        reducers.clearFieldError('content', errorState, errorDispatch);
      }}
    />
    <Button
      className="sm:ml-auto"
      type="submit"
      size="xs"
      text="Submit"
      theme="action"
      loading={loading}
    />
  </form>
);

PostForm.defaultProps = {
  formState: {
    title: '',
    excerpt: '',
    content: '',
    featuredImage: null,
    active: true,
  },
  errorState: {},
  title: 'New Post',
  loading: false,
};

PostForm.propTypes = {
  formState: PropTypes.shape({
    title: PropTypes.string,
    excerpt: PropTypes.string,
    content: PropTypes.string,
    featuredImage: PropTypes.number,
    active: PropTypes.bool,
  }),
  errorState: PropTypes.shape({
    title: PropTypes.string,
    excerpt: PropTypes.string,
    content: PropTypes.string,
  }),
  onSubmit: PropTypes.func.isRequired,
  title: PropTypes.string,
  formDispatch: PropTypes.func.isRequired,
  errorDispatch: PropTypes.func.isRequired,
  loading: PropTypes.bool,
};

export default PostForm;
