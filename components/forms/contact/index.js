import PropTypes from 'prop-types';
import reducers from 'reducers/form-reducer';
import InputField from 'components/core/forms/input-field';
import CheckboxField from 'components/core/forms/checkbox-field';
import DesignIcon from 'assets/svg-components/DesignIcon';
import InfraIcon from 'assets/svg-components/InfraIcon';
import WebIcon from 'assets/svg-components/WebIcon';
import TeaIcon from 'assets/svg-components/TeaIcon';
import SeoIcon from 'assets/svg-components/SeoIcon';
import TextArea from 'components/core/forms/textarea-field';

import Button from 'components/core/buttons/button';

const servicesArray = [
  {
    name: 'web',
    label: 'Web',
    formState: 'web',
    Icon: WebIcon,
    activeClasses: 'fill-current text-purple-600',
  },
  {
    name: 'infra',
    label: 'Infrastructure',
    formState: 'infra',
    Icon: InfraIcon,
    activeClasses: 'fill-current text-purple-600',
  },
  {
    name: 'seo',
    label: 'SEO',
    formState: 'seo',
    Icon: SeoIcon,
    activeClasses: 'fill-current text-purple-600',
  },
  {
    name: 'design',
    label: 'Design',
    formState: 'design',
    Icon: DesignIcon,
    activeClasses: 'fill-current text-purple-600',
  },
  {
    name: 'cuppa',
    label: 'Just facied a wee cuppa.',
    formState: 'cuppa',
    Icon: TeaIcon,
    activeClasses: 'fill-current text-purple-600',
  },
];

const ContactForm = ({
  formState,
  errorState,
  formDispatch,
  errorDispatch,
  onSubmit,
  loading,
}) => (

  <form onSubmit={(e) => onSubmit(e)}>
    <div className="mb-16">
      <h3 className="mb-6"> Hello! 👋 </h3>
      <h6 className="mb-8">What&apos;s your name and where can I email you?</h6>
      <div className="grid grid-cols-1 lg:grid-cols-2 lg:gap-4">
        <InputField
          label="Name"
          name="name"
          placeholder="Name..."
          maxlength={255}
          value={formState.name}
          error={errorState.name}
          required
          onChange={(e) => {
            reducers.formInputHandler(e, formDispatch);
            reducers.clearFieldError('name', errorState, errorDispatch);
          }}
        />
        <InputField
          label="Email"
          name="email"
          type="email"
          placeholder="Email..."
          maxlength={255}
          value={formState.email}
          error={errorState.email}
          required
          onChange={(e) => {
            reducers.formInputHandler(e, formDispatch);
            reducers.clearFieldError('email', errorState, errorDispatch);
          }}
        />
      </div>
    </div>
    <div className="mb-16">
      <h6> Is there anything in particular that I can help with? </h6>
      <p className="mb-12">(Select zero to many.)</p>
      <div className="grid grid-cols-2 sm:grid-cols-2 lg:grid-cols-5 mb-8">
        {
        servicesArray.map((service) => (
          <CheckboxField
            key={service.name}
            label={service.label}
            name={service.name}
            checked={formState[service.formState]}
            Icon={service.Icon}
            onChange={(e) => reducers.formInputHandler(e, formDispatch, { checkbox: true })}
            activeClasses={service.activeClasses}
          />
        ))
      }
      </div>
    </div>
    <div className="mb-16">
      <h6 className="mb-8"> Is there any more info that might be useful?</h6>
      <TextArea
        label="Any other information"
        name="info"
        placeholder="I thought it might be good to know..."
        type="textarea"
        rows={4}
        maxlength={800}
        value={formState.info}
        error={errorState.info}
        onChange={(e) => {
          reducers.formInputHandler(e, formDispatch);
          reducers.clearFieldError('info', errorState, errorDispatch);
        }}
      />
    </div>
    <Button
      className="sm:ml-auto"
      type="submit"
      size="xs"
      text="Submit"
      theme="action"
      loading={loading}
    />
  </form>
);

ContactForm.defaultProps = {
  formState: {
    name: '',
    email: '',
    web: false,
    infra: false,
    seo: false,
    design: false,
    cuppa: false,
    info: '',
  },
  errorState: {},
  loading: false,
};

ContactForm.propTypes = {
  formState: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
    web: PropTypes.bool,
    infra: PropTypes.bool,
    seo: PropTypes.bool,
    design: PropTypes.bool,
    cuppa: PropTypes.bool,
    info: PropTypes.string,
  }),
  errorState: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
    info: PropTypes.string,
  }),
  onSubmit: PropTypes.func.isRequired,
  formDispatch: PropTypes.func.isRequired,
  errorDispatch: PropTypes.func.isRequired,
  loading: PropTypes.bool,
};

export default ContactForm;
