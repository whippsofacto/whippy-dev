import Container from 'components/core/layouts/SlimContainer';
import Image from 'next/image';
import gitlab from 'assets/gitlab-icon-rgb.svg';
import InternalLink from '../links/internal-link';

const date = new Date();
const year = date.getFullYear();
const Footer = () => (
  <footer className="bg-gray-50 py-4">
    <Container>
      <div className="py-10 lg:flex items-end justify-between">
        <div className="mb-12 lg:mb-0">
          <InternalLink href="/" size="unset"> Home </InternalLink>
          <InternalLink href="/about" size="unset"> About </InternalLink>
          <InternalLink href="/contact" size="unset"> Contact </InternalLink>
          <InternalLink href="/bookmarks" size="unset"> Bookmarks </InternalLink>
        </div>
        <div>
          <a href="https://gitlab.com/whippsofacto/"><Image src={gitlab} alt="gitlab" width="32px" height="32px" /></a>
        </div>
      </div>
      <hr />
      <div>
        <div className="py-8 ">
          <p className="bold">
            © Stephen Whipp |
            {' '}
            {year}
          </p>
        </div>
      </div>
    </Container>
  </footer>
);

export default Footer;
