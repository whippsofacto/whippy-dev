import PropTypes from 'prop-types';
import styles from './check-button.module.scss';

const CheckButton = ({
  label, className, active, setActive,
}) => {
  let activeStyles = '';
  if (active) {
    activeStyles = 'bg-indigo-600';
  }

  return (
    <button className={`border-2 border-indigo-600 rounded px-5 py-2 flex items-center ${className} color-indigo-600`} type="button" onClick={setActive}>
      <span className="mr-2">
        {' '}
        {label}
        {' '}
      </span>
      <div className={`${styles['nav-check-box--outside']} border-2 border-indigo-600 relative rounded-full `}>
        <span className={`${styles['nav-check-box--inside']} --inside  border border-indigo-600 absolute rounded-full transition ease-in ${activeStyles}`} />
      </div>
    </button>
  );
};

CheckButton.defaultProps = {
  className: '',
};

CheckButton.propTypes = {
  label: PropTypes.string.isRequired,
  className: PropTypes.string,
  active: PropTypes.bool.isRequired,
  setActive: PropTypes.func.isRequired,
};

export default CheckButton;
