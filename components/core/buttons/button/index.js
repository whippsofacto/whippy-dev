/* eslint  react/button-has-type: 0 */

import PropTypes from 'prop-types';

const colors = {
  standard: 'bg-gray-800 hover:bg-gray-700 focus:bg-gray-700',
  action: 'bg-indigo-600 hover:bg-indigo-500 focus:bg-indigo-500',
  success: 'bg-green-500 hover:bg-green-400 focus:bg-green-400',
  danger: 'bg-red-500 hover:bg-red-400 focus:bg-red-400',
};

const sizes = {
  xs: 'sm:max-w-xs',
  sm: 'sm:max-w-sm',
  md: 'sm:max-w-md',
  lg: 'sm:max-w-lg',
  fw: 'w-full',
};

const Button = ({
  loading, text, theme, size, type, className, onClick,
}) => (
  <button
    onClick={onClick}
    disabled={!!(loading)}
    type={type}
    className={`
        ${(loading && 'cursor-not-allowed')}
        ${className}
        flex justify-center text-white py-2  ${sizes[size]} w-full
        px-3 rounded focus:outline-none ${colors[theme]}"
    `}
  >
    {
        loading && (
        <svg className="animate-spin -ml-1 mr-3 h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
          <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4" />
          <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z" />
        </svg>
        )
    }
    {text}
  </button>
);

Button.defaultProps = {
  loading: false,
  text: '',
  theme: 'standard',
  size: 'sm',
  type: 'button',
  className: '',
  onClick: () => {},
};
Button.propTypes = {
  loading: PropTypes.bool,
  text: PropTypes.string,
  theme: PropTypes.string,
  size: PropTypes.string,
  type: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
};
export default Button;
