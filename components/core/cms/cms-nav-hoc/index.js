import PropTypes from 'prop-types';

import SideBar from 'components/core/cms/sidebar';
import TopBar from 'components/core/cms/topbar';

export default function Cms({ children }) {
  return (
    <>
      <TopBar />
      <div>
        <SideBar />
        <div className="mb-12 sm:mb-0  sm:ml-36 p-5">
          <div className="container">
            {children}
          </div>
        </div>
      </div>
    </>
  );
}

Cms.propTypes = {
  children: PropTypes.node.isRequired,
};
