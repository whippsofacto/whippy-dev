import logo from 'assets/woodblock-wip.svg';
import brand from 'assets/woodbloc-icon.svg';
import InternalLink from 'components/core/links/internal-link';
import MobileMenu from 'components/core/cms/mobile-menu';
import styles from './topbar.module.scss';

const TopBar = () => (
  <>
    <div className="w-screen shadow-md py-2 fixed z-10 bg-white bottom-0 sm:bottom-auto border-t-2 border-gray-100 sm:border-0">
      <div className="flex items-center mx-5">
        <InternalLink href="/">
          <img
            src={logo}
            alt="brand"
            className="hidden sm:inline-flex"
          />
          <img
            className="inline-flex sm:hidden"
            src={brand}
            alt="brand"
          />
        </InternalLink>
        <div className="inline-block sm:hidden ml-auto">
          <MobileMenu />
        </div>
        <div className="user-profile ml-auto hidden sm:inline-flex">
          <InternalLink href="/logout">
            <img
              className="w-12 h-12 rounded-full object-cover"
              src="https://resizing.flixster.com/yy-arZ1YpXOAxsVCuR3VNSCTnK0=/506x652/v2/https://flxt.tmsimg.com/v9/AllPhotos/656/656_v9_bb.jpg"
              alt="profle"
            />
          </InternalLink>
        </div>
      </div>
    </div>
    <div className={`${styles['cms-nav-height']} hidden sm:inline-block`} />
  </>
);

export default TopBar;
