import CMSNavLinks from 'components/core/cms/nav-links';

const SideBar = () => (
  <>
    <div className="fixed h-screen w-36 bg-gray-50 shadow-md z-0 py-5 hidden sm:block">
      <nav className="flex flex-col">
        <CMSNavLinks />
      </nav>
    </div>
  </>
);

export default SideBar;
