import { useState } from 'react';
import NavLinks from 'components/core/cms/nav-links';
import close from 'assets/close.svg';
import styles from './mobile-menu.module.scss';

const MobileMenu = () => {
  const [nav, showNav] = useState(false);

  return (
    <>
      <button type="button" className={`${styles.hamburger}`} onClick={() => showNav(!nav)}>
        <span className={`${styles.slice} bg-primary`} />
        <span className={`${styles.slice} bg-primary`} />
        <span className={`${styles.slice} bg-primary`} />
      </button>
      <div className={`${styles['cms-nav-links']} ${nav ? styles.active : ''} bg-white`}>
        <NavLinks />
        <div className={`px-4 flex w-full bottom-0 left-0 mt-auto ${nav ? 'sticky ' : ''}`}>
          <button className="ml-auto" type="button" onClick={() => showNav(!nav)}>
            {' '}
            <img className="py-4 pl-4" alt="close nav" src={close} />
            {' '}
          </button>
        </div>
      </div>
    </>
  );
};
export default MobileMenu;
