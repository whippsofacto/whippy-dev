import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import InternalLink from 'components/core/links/internal-link';

const CMSNavLink = ({ href, label }) => {
  const router = useRouter();

  let active = 'text-gray-600 hover:bg-purple-100';
  if (router.pathname.indexOf(href) > -1) {
    active = 'bg-primary text-white';
  }

  return (
    <InternalLink href={href}>
      <div className={`pl-4 py-2 ${active}`}>
        {label}
      </div>
    </InternalLink>
  );
};

CMSNavLink.propTypes = {
  href: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

const CMSNavLinks = () => (
  <>
    <CMSNavLink href="/cms/dashboard" label="Dashboard" />
    <CMSNavLink href="/cms/posts" label="Posts" />
  </>
);

export default CMSNavLinks;
