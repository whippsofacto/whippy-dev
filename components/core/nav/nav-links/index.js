import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import InternalLink from 'components/core/links/internal-link';
import SlimContainer from 'components/core/layouts/SlimContainer';

const CMSNavLink = ({ href, label }) => {
  const router = useRouter();

  let active = 'text-gray-600 hover:bg-purple-100';

  if (router.pathname === href) {
    active = 'bg-primary text-white';
  }

  return (
    <InternalLink href={href}>
      <div className={`pl-4 py-2 rounded ${active}`}>
        {label}
      </div>
    </InternalLink>
  );
};

CMSNavLink.propTypes = {
  href: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

const CMSNavLinks = () => (
  <div className="w-full h-full flex flex-col items-center justify-center">
    <SlimContainer>
      <CMSNavLink href="/" label="Home" />
      <CMSNavLink href="/about" label="About" />
      <CMSNavLink href="/contact" label="Contact" />
    </SlimContainer>
  </div>
);

export default CMSNavLinks;
