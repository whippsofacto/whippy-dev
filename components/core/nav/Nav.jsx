import { useState } from 'react';
import close from 'assets/close.svg';
import NavLinks from './nav-links';
import styles from './nav.module.scss';

const MobileMenu = () => {
  const [nav, showNav] = useState(false);

  return (
    <>
      <button type="button" className={`${styles.hamburger}`} onClick={() => showNav(!nav)}>
        <span className={`${styles.slice} bg-primary`} />
        <span className={`${styles.slice} bg-primary`} />
        <span className={`${styles.slice} bg-primary`} />
      </button>
      <div className={`${styles['cms-nav-links']} ${nav ? styles.active : ''} bg-white`}>
        <NavLinks />
        <div className="px-4 flex w-full top-2 right-2 mt-auto absolute ">
          <button className="ml-auto" type="button" onClick={() => showNav(!nav)}>
            {' '}
            <img className="py-4 pl-4" alt="close nav" src={close} />
            {' '}
          </button>
        </div>
      </div>
    </>
  );
};
export default MobileMenu;
