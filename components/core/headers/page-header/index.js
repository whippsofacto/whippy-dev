import PropTypes from 'prop-types';
import Container from 'components/core/layouts/Container';

const PageHeader = ({ backgroundImage, heading }) => {
  const style = {};
  if (backgroundImage) {
    style.backgroundImage = `url(${backgroundImage})`;
  }
  return (
    <div
      className={`
          bg-contain bg-repeat
          mb-10
          `}
      style={style}
    >
      <Container>
        <div className="flex flex-col justify-center items-center mb-12 py-32">
          <div className="flex">
            <h2 className="p-4 border-solid border-b-8">
              {' '}
              {heading}
              {' '}
            </h2>
          </div>
        </div>
      </Container>
    </div>
  );
};

PageHeader.defaultProps = {
  backgroundImage: undefined,
};

PageHeader.propTypes = {
  backgroundImage: PropTypes.string,
  heading: PropTypes.string.isRequired,
};

export default PageHeader;
