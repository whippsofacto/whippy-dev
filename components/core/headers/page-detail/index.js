import PropTypes from 'prop-types';
import Container from 'components/core/layouts/SlimContainer';

const PageDetail = ({ backgroundImage, heading }) => {
  const style = {};
  if (backgroundImage) {
    style.backgroundImage = `url(${backgroundImage})`;
  }
  return (
    <div
      className={`
          bg-contain bg-repeat
          mb-10
          `}
      style={style}
    >
      <Container>
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:gap-8 my-20 lg:mt-32 lg:mb-32">
          <h2 className="p-4 border-solid border-b-8">
            {' '}
            {heading}
            {' '}
          </h2>
        </div>
      </Container>
    </div>
  );
};

PageDetail.defaultProps = {
  backgroundImage: undefined,
};

PageDetail.propTypes = {
  backgroundImage: PropTypes.string,
  heading: PropTypes.string.isRequired,
};

export default PageDetail;
