import PropTypes from 'prop-types';

const Container = ({ children }) => (
  <div className="container mx-auto px-4 ">
    {children}
  </div>
);

Container.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default Container;
