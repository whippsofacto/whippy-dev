import PropTypes from 'prop-types';
import Nav from 'components/core/nav/Nav';
import Footer from '../footer/Footer';

const Main = ({ children }) => (
  <>
    <Nav />
    {children}
    <Footer />
  </>
);

Main.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default Main;
