import React from 'react';
import PropTypes from 'prop-types';

const SlimContainer = React.forwardRef(({ children }, ref) => (
  <div ref={ref} className="container mx-auto px-4 sm:px-32 md:px-48 lg:px-32 xl:px-72 2xl:px-90">
    {children}
  </div>
));

SlimContainer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default SlimContainer;
