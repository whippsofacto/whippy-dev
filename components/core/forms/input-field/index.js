import PropTypes from 'prop-types';

const InputField = ({
  id, type, name, placeholder, onChange, error, label, maxlength, genericInputFont, value, required,
}) => (
  <div className="my-4 text-sm">
    <label htmlFor={name} className="block text-black font-semibold mb-3">{label}</label>
    <input
      type={type}
      id={id}
      name={name}
      onChange={onChange}
      maxLength={maxlength}
      required={required}
      className={
              `
               ${genericInputFont ? 'font-sans text-cms-input' : ''}
               rounded-sm px-4 py-3 mb-1 focus:outline-none bg-gray-100 w-full
               ${error && 'bg-red-100'}
              `
              }
      placeholder={placeholder}
      value={value}
    />
    <span className="mb-2 text-red-400">
      { error && error}
    </span>
  </div>
);

InputField.defaultProps = {
  id: '',
  type: 'text',
  placeholder: '',
  onChange: () => {},
  error: '',
  maxlength: undefined,
  genericInputFont: true,
  required: false,
};

InputField.propTypes = {
  id: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  error: PropTypes.string,
  label: PropTypes.string.isRequired,
  maxlength: PropTypes.number,
  genericInputFont: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.string]).isRequired,
  required: PropTypes.bool,
};

export default InputField;
