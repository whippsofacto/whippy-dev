import PropTypes from 'prop-types';
import { Editor } from '@tinymce/tinymce-react';
import mediaService from 'services/client/media/media';

const RichTextEditor = ({
  placeholder, initValue, id, onChange,
}) => (
  <Editor
    id={id}
    name={id}
    initialValue={initValue}
    onChange={onChange}
    init={{
      placeholder,
      height: 500,
      menubar: false,
      images_upload_handler: mediaService.richTextMediaUploadHandler,
      relative_urls: false,
      plugins: [
        'advlist autolink lists link image',
        'charmap print preview anchor help',
        'searchreplace visualblocks code',
        'insertdatetime media table paste wordcount',
        'codesample',
      ],
      toolbar:
            'undo redo | formatselect | codesample | bold italic | image | link | alignleft aligncenter alignright | bullist numlist outdent indent | help',
    }}
  />
);

RichTextEditor.defaultProps = {
  placeholder: '',
  initValue: '',
  onChange: () => {},
};

RichTextEditor.propTypes = {
  placeholder: PropTypes.string,
  initValue: PropTypes.string,
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func,
};

export default RichTextEditor;
