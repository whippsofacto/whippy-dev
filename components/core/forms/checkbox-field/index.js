import PropTypes from 'prop-types';
import CheckBox from '../chekbox';

const CheckboxField = ({
  label, name, checked, onChange, Icon, activeClasses,
}) => (
  <div className="relative text-center mb-4 lg:mb-0">
    {
        Icon
        && (
        <div className="mb-4">
          <button type="button" name={name}>
            <Icon className={(checked) ? activeClasses : ''} />
          </button>
        </div>
        )
    }
    <label>
      {label}
    </label>
    <div className="absolute top-0 left-0 w-full h-full bg-opacity-0">
      <CheckBox name={name} checked={checked} onChange={onChange} className="w-full h-full cursor-pointer opacity-0" />
    </div>
  </div>
);

CheckboxField.defaultProps = {
  Icon: undefined,
  checked: false,
  activeClasses: '',
};

CheckboxField.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  checked: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  onChange: PropTypes.func.isRequired,
  Icon: PropTypes.func,
  activeClasses: PropTypes.string,
};

export default CheckboxField;
