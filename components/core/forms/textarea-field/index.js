import PropTypes from 'prop-types';

const TextArea = ({
  id, name, placeholder, onChange, error, label, rows, maxlength, genericInputFont, value,
}) => (
  <div className="my-5 text-sm">
    <label htmlFor={name} className="block text-black font-semibold mb-3">{label}</label>
    <textarea
      id={id}
      name={name}
      onChange={onChange}
      className={
              `
               ${genericInputFont ? 'font-sans text-cms-input' : ''}
               rounded-sm px-4 py-3 mb-1 focus:outline-none bg-gray-100 w-full
               ${error && 'bg-red-100'}
              `
              }
      placeholder={placeholder}
      rows={rows}
      maxLength={maxlength}
      value={value}
    />
    {maxlength
      && (
      <p className="text-right">
        Characters remaining:
        {' '}
        {maxlength - value.length}
        {' '}
      </p>
      )}
    <span className="mb-2 text-red-400">
      { error && error}
    </span>
  </div>
);

TextArea.defaultProps = {
  id: '',
  placeholder: '',
  onChange: () => {},
  error: '',
  rows: 4,
  maxlength: undefined,
  genericInputFont: true,
  value: '',
};

TextArea.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  error: PropTypes.string,
  label: PropTypes.string.isRequired,
  rows: PropTypes.number,
  maxlength: PropTypes.number,
  genericInputFont: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.string]),
};

export default TextArea;
