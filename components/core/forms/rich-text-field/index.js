import PropTypes from 'prop-types';
import RichTextEditor from 'components/core/forms/rich-text';
import styles from './rich-text-field.module.scss';

const RichTextField = (
  {
    name, label, error, onChange, placeholder, id, value,
  },
) => (
  <div className={`my-5 text-sm ${error ? styles.error : ''}`}>
    <label htmlFor={name} className="block text-black font-semibold mb-3">{label}</label>
    <div className="mb-4">
      <RichTextEditor
        id={id}
        onChange={onChange}
        placeholder={placeholder}
        initValue={value}
      />
    </div>
    <span className="mb-2 text-red-400">
      { error && error}
    </span>
  </div>
);

RichTextField.defaultProps = {
  placeholder: '',
  onChange: () => {},
  error: '',
  value: '',
};

RichTextField.propTypes = {
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  error: PropTypes.string,
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.string]),
};

export default RichTextField;
