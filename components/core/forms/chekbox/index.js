import PropTypes from 'prop-types';

const CheckBox = ({
  name, onChange, checked, className,
}) => (
  <input type="checkbox" name={name} checked={checked} onChange={onChange} className={className} />
);

CheckBox.defaultProps = {
  checked: false,
  className: '',
};

CheckBox.propTypes = {
  checked: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
};

export default CheckBox;
