import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Switch from 'react-switch';

const ToggleSwitch = ({
  onChange, value, id, className, label,
}) => {
  const [checked, updateChecked] = useState(true);

  useEffect(() => {
    updateChecked(value);
  }, []);

  return (
    <div className={className}>
      <label htmlFor="switch">
        {label && <span>{label}</span>}
        <Switch
          id={id}
          name="switch"
          onColor="#91e4d7"
          onHandleColor="#FFF"
          handleDiameter={30}
          uncheckedIcon={false}
          checkedIcon={false}
          boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
          activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
          onChange={() => {
            updateChecked(!checked);
            onChange(!checked);
          }}
          checked={checked}
        />
      </label>
    </div>
  );
};

ToggleSwitch.defaultProps = {
  value: true,
  id: 'switch',
  className: '',
  label: '',
};

ToggleSwitch.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.bool,
  id: PropTypes.string,
  className: PropTypes.string,
  label: PropTypes.string,
};

export default ToggleSwitch;
