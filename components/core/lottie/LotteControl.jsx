import { useState } from 'react';
import PropTypes from 'prop-types';
import Lottie from 'react-lottie';

const LottieControl = ({
  animationData, playerControls, height, width, display, className,
}) => {
  const [isStopped, updateIsStopped] = useState(false);
  const [isPaused, updateIsPaused] = useState(false);

  const buttonStyle = {
    display: 'block',
    margin: '10px auto',
  };

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice',
    },
  };

  return (
    <>
      {
       display && (
       <div className={className}>
         <Lottie
           options={defaultOptions}
           height={height}
           width={width}
           isStopped={isStopped}
           isPaused={isPaused}
           style={{ margin: 0 }}
           isClickToPauseDisabled
         />
         {
            playerControls
            && (
            <>
              <button type="button" style={buttonStyle} onClick={() => updateIsStopped(true)}>stop</button>
              <button type="button" style={buttonStyle} onClick={() => updateIsStopped(false)}>play</button>
              <button type="button" style={buttonStyle} onClick={() => updateIsPaused(!isPaused)}>pause</button>
            </>
            )
          }
       </div>
       )
      }

    </>

  );
};

LottieControl.defaultProps = {
  playerControls: false,
  height: 200,
  width: 200,
  display: true,
  className: '',
};

LottieControl.propTypes = {
  className: PropTypes.string,
  playerControls: PropTypes.bool,
  height: PropTypes.number,
  width: PropTypes.number,
  display: PropTypes.bool,
  animationData: PropTypes.shape({}).isRequired,
};

export default LottieControl;
