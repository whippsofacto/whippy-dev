import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Uploader from 'components/core/uploaders/Uploader';
import mediaService from 'services/client/media/media';
import Image from 'components/core/image/Image';

const MediaUploader = ({ id, onChange, value }) => {
  const [file, getFile] = useState(null);
  const [uploadedMediaId, updateUploadedMediaID] = useState(null);

  useEffect(() => {
    updateUploadedMediaID(value);
  }, []);

  useEffect(() => {
    const postMedia = async () => {
      const mediaId = await mediaService.postMedia(file);
      onChange(mediaId);
      updateUploadedMediaID(mediaId);
      getFile(null);
    };

    if (file) {
      postMedia();
    }
  }, [file]);

  const handleClearingImage = () => {
    getFile(null);
    updateUploadedMediaID(null);
    onChange(null);
  };

  return (
    <div className="mb-6">

      { (uploadedMediaId)
        ? (
          <>
            <Image src={`/api/media/${uploadedMediaId}`} alt="media" />
            <button type="button" onClick={handleClearingImage}> X </button>
          </>
        )
        : (
          <Uploader getFiles={getFile} maxFiles={1} id={id} allowedFiles="image/*" />
        )}
    </div>
  );
};

MediaUploader.defaultProps = {
  value: null,
};

MediaUploader.propTypes = {
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.number,
};

export default MediaUploader;
