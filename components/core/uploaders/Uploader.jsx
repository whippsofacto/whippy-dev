/* eslint-disable react/jsx-props-no-spreading */
import { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';

const Uploader = ({
  getFiles, maxFiles, id, allowedFiles,
}) => {
  const onDrop = useCallback((acceptedFiles) => {
    if (acceptedFiles.length) {
    // Do something with the files
      getFiles(acceptedFiles);
    }
  }, []);

  const dropZoneOptions = {};
  if (allowedFiles) {
    dropZoneOptions.accept = allowedFiles;
  }
  if (maxFiles) {
    dropZoneOptions.maxFiles = maxFiles;
  }

  const { getRootProps, getInputProps, isDragActive } = useDropzone(
    {
      onDrop,
      ...dropZoneOptions,
    },
  );

  return (
    <div id={id} {...getRootProps()}>
      <input {...getInputProps()} />
      {
        isDragActive
          ? <p>Drop the files here ...</p>
          : <p>Drag 'n' drop some files here, or click to select files</p>
      }
    </div>
  );
};

Uploader.defaultProps = {
  maxFiles: null,
  id: 'fileUploader',
  allowedFiles: null,
};

Uploader.propTypes = {
  getFiles: PropTypes.func.isRequired,
  maxFiles: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  allowedFiles: PropTypes.string,
  id: PropTypes.string,
};

export default Uploader;
