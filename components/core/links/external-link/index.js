import PropTypes from 'prop-types';
import icon from 'assets/open-in-new-window.svg';
import Image from 'next/image';

const ExternalLink = ({ link, linkText }) => (
  <a className="underline text-indigo-600" href={link} target="_blank" rel="noreferrer">
    {linkText}
    <span className="ml-1">
      <Image src={icon} alt="external link" width="16px" height="16px" />
    </span>
  </a>

);

ExternalLink.propTypes = {
  link: PropTypes.string.isRequired,
  linkText: PropTypes.string.isRequired,
};
export default ExternalLink;
