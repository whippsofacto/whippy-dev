/* eslint jsx-a11y/anchor-is-valid: 0 */

import Link from 'next/link';
import PropTypes from 'prop-types';

const sizes = {
  xs: 'sm:max-w-xs',
  sm: 'sm:max-w-sm',
  md: 'sm:max-w-md',
  lg: 'sm:max-w-lg',
  fw: 'w-full',
  unset: '',
};

const InternalLink = ({
  href, children, size, className,
}) => (
  <div className={`${sizes[size]} ${className}`}>
    <Link href={href}>
      <a>
        {children}
      </a>
    </Link>
  </div>
);

export default InternalLink;

InternalLink.defaultProps = {
  size: 'fw',
  className: '',
};
InternalLink.propTypes = {
  children: PropTypes.node.isRequired,
  href: PropTypes.string.isRequired,
  size: PropTypes.string,
  className: PropTypes.string,
};
