import ToEmail from 'components/core/modals/templates/to-email';

const templates = {
  toEmail: ToEmail,
};

export default templates;
