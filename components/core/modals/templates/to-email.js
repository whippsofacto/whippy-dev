const ToEmail = (
  <a href="https://gmail.com">
    <svg width="252px" height="46px" viewBox="0 0 252 46" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <title>button</title>
      <desc>link to gmail</desc>
      <defs>
        <rect id="path-1" x="0" y="0" width="248" height="40" rx="2" />
        <filter x="-1.6%" y="-10.0%" width="103.2%" height="125.0%" filterUnits="objectBoundingBox" id="filter-2">
          <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1" />
          <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
          <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.24 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1" />
          <feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter2" />
          <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter2" result="shadowBlurOuter2" />
          <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.12 0" type="matrix" in="shadowBlurOuter2" result="shadowMatrixOuter2" />
          <feMerge>
            <feMergeNode in="shadowMatrixOuter1" />
            <feMergeNode in="shadowMatrixOuter2" />
          </feMerge>
        </filter>
      </defs>
      <g id="Google-Button" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g id="Google-Sign-in-example" transform="translate(-54.000000, -420.000000)">
          <g id="button" transform="translate(56.000000, 422.000000)">
            <g id="button-bg">
              <use fill="black" fillOpacity="1" filter="url(#filter-2)" xlinkHref="#path-1" />
              <use fill="#FFFFFF" fillRule="evenodd" xlinkHref="#path-1" />
            </g>
            <text id="Sign-in-with-Google" fontFamily="Helvetica" fontSize="14" fontWeight="normal" letterSpacing="0.21875" fill="#000000" fillOpacity="0.54">
              <tspan x="83" y="26">Go to Gmail</tspan>
            </text>
            <g id="super-g" fill="#000000" transform="translate(11.000000, 11.000000)">
              <path d="M9,3.48 C10.69,3.48 11.83,4.21 12.48,4.82 L15.02,2.34 C13.46,0.89 11.43,0 9,0 C5.48,0 2.44,2.02 0.96,4.96 L3.87,7.22 C4.6,5.05 6.62,3.48 9,3.48 L9,3.48 Z" id="Shape" fill="#EA4335" />
              <path d="M17.64,9.2 C17.64,8.46 17.58,7.92 17.45,7.36 L9,7.36 L9,10.7 L13.96,10.7 C13.86,11.53 13.32,12.78 12.12,13.62 L14.96,15.82 C16.66,14.25 17.64,11.94 17.64,9.2 L17.64,9.2 Z" id="Shape" fill="#4285F4" />
              <path d="M3.88,10.78 C3.69,10.22 3.58,9.62 3.58,9 C3.58,8.38 3.69,7.78 3.87,7.22 L0.96,4.96 C0.35,6.18 0,7.55 0,9 C0,10.45 0.35,11.82 0.96,13.04 L3.88,10.78 L3.88,10.78 Z" id="Shape" fill="#FBBC05" />
              <path d="M9,18 C11.43,18 13.47,17.2 14.96,15.82 L12.12,13.62 C11.36,14.15 10.34,14.52 9,14.52 C6.62,14.52 4.6,12.95 3.88,10.78 L0.97,13.04 C2.45,15.98 5.48,18 9,18 L9,18 Z" id="Shape" fill="#34A853" />
              <polygon id="Shape" fill="none" points="0 0 18 0 18 18 0 18" />
            </g>
            <g id="gmail" transform="translate(9.000000, 8.000000)" fillRule="nonzero">
              <g id="Group" transform="translate(2.000000, 0.000000)" fill="#F2F2F2">
                <polygon id="Path" points="26.9983872 3.84538776 25.0459646 23.9398163 1.59562832 23.9398163 0.0226393805 4.23428571 13.3207965 11.8654286" />
                <polygon id="Path" points="26.3361106 0.0601836735 13.3207965 12.5913673 0.305482301 0.0601836735 13.3207965 0.0601836735" />
              </g>
              <path d="M4,5.69590326 L4,24 L1.6170284,24 C0.724276527,24 0,23.3303908 0,22.5048842 L0,3 L2.61254019,3.06583985 L4,5.69590326 Z" id="Path" fill="#F14336" />
              <path d="M31,3.1841222 L31,22.5179974 C31,23.3362023 30.2757235,24 29.3821008,24 L27,24 L27,5.85638065 L28.3191318,3 L31,3.1841222 Z" id="Path" fill="#D32E2A" />
              <path d="M31,1.52406789 L31,3.21898492 L27.384625,5.96709291 L15.5,15 L3.615375,5.96709291 L0,3.21898492 L0,1.52406789 C0,0.682638965 0.654632812,0 1.46154102,0 L2.30768359,0 L15.5,10.0267491 L28.6923164,0 L29.5376719,0 C30.3453672,0 31,0.682575828 31,1.52406789 Z" id="Path" fill="#F14336" />
              <polygon id="Path" fill="#D32E2A" points="4 6 0 5.36189864 0 3" />
            </g>
          </g>
        </g>
      </g>
    </svg>
  </a>
);

export default ToEmail;
