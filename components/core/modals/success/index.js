import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import templates from 'components/core/modals/templates';
import Button from 'components/core/buttons/button';

const Success = ({
  message, template, active, onClick, cta,
}) => {
  const [show, updateShow] = useState(false);

  useEffect(() => {
    updateShow(active);
  }, [active]);

  let TemplateFragment;
  if (template) {
    TemplateFragment = () => templates[template];
  }

  const transition = (show) ? 'ease-out duration-300 opacity-100' : 'ease-in duration-200 opacity-0';

  return (
    <>
      <div className={`fixed ${(show) ? 'z-10' : '-z-10'} inset-0 overflow-y-auto`}>
        <div className="flex items-center justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">

          <div className={`fixed inset-0 transition-opacity ${transition}`} aria-hidden="true">
            <div className="absolute inset-0 bg-gray-500 opacity-75" />
          </div>

          <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

          <div
            className={`
            inline-block align-bottom bg-white
            rounded-lg text-left
            overflow-hidden shadow-xl transform transition-all
            sm:my-8 sm:align-middle sm:max-w-lg sm:w-full
            ${transition}
           translate-y-4 sm:translate-y-0 sm:scale-95
          `}
            role="dialog"
            aria-modal="true"
            aria-labelledby="modal-headline"
          >
            <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
              <div className="flex items-center justify-center flex-col">
                <div className="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-green-100 sm:mx-0 sm:h-10 sm:w-10">
                  <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                      <circle fill="#DEF7EC" cx="27" cy="27" r="26" />
                      <polyline stroke="#1D8766" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" points="20 28.0350634 24.3340869 32 35 22" />
                    </g>
                  </svg>
                </div>
                <div className="mt-3 text-center">
                  <h3 className="text-lg leading-6 font-medium text-gray-900" id="modal-headline">
                    Success
                  </h3>
                  <div className="mt-2">
                    <p className="text-sm text-gray-500">
                      {message}
                    </p>
                  </div>
                </div>
                {
                    template && (
                    <div className="mt-5">
                      <TemplateFragment />
                    </div>
                    )
                  }
                {
                    cta && (
                    <div className="mt-5 w-full flex justify-center">
                      <Button onClick={onClick} text={cta} theme="success" />
                    </div>
                    )
                  }

              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
Success.defaultProps = {
  message: '',
  template: undefined,
  onClick: () => {},
  cta: '',
};

Success.propTypes = {
  message: PropTypes.string,
  template: PropTypes.string,
  active: PropTypes.bool.isRequired,
  onClick: PropTypes.func,
  cta: PropTypes.string,
};

export default Success;
