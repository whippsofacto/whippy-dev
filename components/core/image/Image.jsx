import { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import LottieLoader from 'components/core/lottie/LotteControl';
import animationData from 'assets/lottie/white-image-loader.json';

const Image = ({ src, alt, className }) => {
  const [loading, updateLoading] = useState(true);
  const image = useRef();
  useEffect(() => {
    if (image.current.complete) updateLoading(false);
  }, []);

  return (
    <>
      <LottieLoader
        className={`${className} flex items-center justify-center`}
        animationData={animationData}
        display={loading}
      />
      <img ref={image} className={className} alt={alt} src={src} onLoad={() => updateLoading(false)} style={{ display: (loading) ? 'none' : 'block' }} />
    </>

  );
};

Image.defaultProps = {
  className: '',
};

Image.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default Image;
