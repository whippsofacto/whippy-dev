import PropTypes from 'prop-types';
import SanitizedHTML from 'react-sanitized-html';
import styles from './safehtml.module.scss';

const SafeHtml = ({ html }) => (
  <div className={styles['safe-html']}>
    <SanitizedHTML
      allowedAttributes={
        {
          a: ['href', 'target'],
          img: ['src', 'alt'],
          p: ['style'],
          pre: ['class'],
        }
}
      allowedTags={
        [
          'a',
          'img',
          'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
          'p',
          'em',
          'strong',
          'ul',
          'ol',
          'li',
          'pre',
          'code',
        ]
    }
      html={html}
    />
  </div>
);

SafeHtml.propTypes = {
  html: PropTypes.node.isRequired,
};

export default SafeHtml;
