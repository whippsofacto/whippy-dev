import InternalLink from 'components/core/links/internal-link';
import fallback from 'public/fallback-post-image-min.png';
import Image from 'components/core/image/Image';
import PropTypes from 'prop-types';

const calulateClasses = (val, iffed, elsed) => (((val + 1) % 2 === 0) ? iffed : elsed);
const HomePostColummns = ({ posts }) => (
  <>
    {
    posts.map((post, index) => (
      <div
        key={post.id}
        className={
                  `mb-10 lg:mb-20
                   flex
                   lg:justify-center
                   lg:items-center
                   flex-col-reverse
                  ${calulateClasses(index, 'lg:flex-row lg:justify-start', 'lg:flex-row-reverse lg:justify-start')}
                  `
                  }
      >
        <div className={`
                    lg:w-2/4
                    ${calulateClasses(index, 'lg:items-end lg:pr-10', 'lg:items-start lg:pl-10 text-align')}
                  `}
        >
          <h5>
            <InternalLink href={`/post/${post.id}`}>
              {post.title}
            </InternalLink>
          </h5>
          <p className="mb-4">
            {post.excerpt}
          </p>
          <InternalLink className="underline text-indigo-600" href={`/post/${post.id}`}> Read More </InternalLink>
        </div>
        <Image
          src={(post.featuredImage) ? `/api/media/${post.featuredImage}` : fallback}
          alt={post.title}
          className={`
                    lg:w-2/4
                    max-h-80 sm:max-h-96 sm:w-96 sm:h-96 object-cover rounded lg:max-w-none
                    ${calulateClasses(index, 'lg:ml-auto', 'lg:mr-auto')}
                    `}
        />
      </div>

    ))
    }
  </>
);

HomePostColummns.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

export default HomePostColummns;
